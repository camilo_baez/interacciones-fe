'use strict';

angular.module('sbAdminApp')
  .controller('SeveridadListController', function($scope, severidadService) {

    $scope.config = {
      title    : 'Severidades',
      detail   : 'Lista de severidades del sistema',
      resource : 'severidad',
      rowLabel : 'nombre',
      icon :  'fa fa-exclamation-triangle fa-fw'
    };

    $scope.service = severidadService;

    $scope.columns = [
      { title: 'Nombre', data: 'nombre'},
      { title: 'Descripción', data: 'descripcion'},
      { title: 'Nivel', data: 'nivel', width: '20%'}
    ];

  });

angular.module('sbAdminApp')
  .controller('SeveridadController', function($scope, $stateParams, $timeout, growl, $location, severidadService) {

    $scope.service = severidadService;
    $scope.config = {
      title :'Severidad',
      icon :  'fa fa-exclamation-triangle fa-fw'
    };
    $scope.form = [
      {
        key         : 'nombre',
        type        : 'string',
        placeholder : 'Nombre de la severidad',
        title       : 'Nombre'
      },
      {
        key         : 'descripcion',
        type        : 'textarea',
        placeholder : 'Descripción de la severidad',
        title       : 'Descripción'
      },
      {
        key         : 'nivel',
        type        : 'number',
        placeholder : 'Nivel de la severidad',
        title       : 'Nivel', default:1, description: 'Grado de importancia'
      }];
    /*
     validationMessage: {
     0: "Campo numérico" //numerico
     }
     */

  });
