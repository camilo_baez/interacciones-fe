'use strict';

angular.module('sbAdminApp')
.controller('ActivoListController', function($scope, activoService) {

  $scope.config = {
    title    : 'Activos',
    detail   : 'Lista de activos del sistema',
    resource : 'activo',
    rowLabel : 'nombre',
    icon : 'fa fa-bullseye fa-fw', cargaExcel: true
  };

  $scope.service = activoService;

  $scope.columns = [
    { title: 'Nombre'    , data: 'nombre', width: '25%'},
    { title: 'Descripción', data: 'descripcion'},
    { title: 'DAR', data: 'altoRiesgo',  width: '10%',
      render: function(value, display, data) { return data.altoRiesgo === true? 'SI': 'NO' ;} ,
      filter_config: function (base) {
      return angular.extend(base, {
        data                : [{value: true, label:'SI'}, {value: false, label:'NO'}]
      });
    }}
  ];

});

angular.module('sbAdminApp')
  .controller('ActivoController', function($scope, $stateParams, $timeout, growl, $location, activoService) {
    $scope.localModel = {};
    $scope.localModel.altoRiesgo = false;
    function configurar (model){
      $scope.localModel.altoRiesgo = model.altoRiesgo || false;
    }
    $scope.service = activoService;
    $scope.config = {
      title :'Activo',
      icon : 'fa fa-bullseye fa-fw',
      preSubmit: function(model) {
        model.altoRiesgo = $scope.localModel.altoRiesgo;
      },
      afterLoad: function(model){
        configurar(model);
      }
    };
    $scope.form = [
      {
        key         : 'nombre',
        type        : 'string',
        placeholder : 'Nombre del activo',
        title       : 'Nombre'
      },
      {
        key         : 'descripcion',
        type        : 'textarea',
        placeholder : 'Descripcion del activo',
        title       : 'Descripcion'
      }];
    /*
     validationMessage: {
     0: "Campo numérico" //numerico
     }
     */

  });
