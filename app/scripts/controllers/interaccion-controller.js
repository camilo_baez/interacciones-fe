'use strict';

angular.module('sbAdminApp')
.controller('InteraccionListController', function($scope, interaccionService, activoService, severidadService) {
  var inicializarFiltros = function (oTable, filter) {
    activoService.getAll().then(
      function ok(response) {
        var datos = [];
        angular.forEach(response.data, function(activo) {
          datos.push({value: activo.id, label:activo.nombre});
        });
        filter[0].data = datos;
        filter[1].data = datos;
        yadcf.init(oTable, filter);
      });
    severidadService.getAll().then(
      function ok(response) {
        var datos = [];
        angular.forEach(response.data, function(activo) {
          datos.push({value: activo.id, label:activo.nombre});
        });
        filter[2].data = datos;
        yadcf.init(oTable, filter);
      });
  };

  $scope.config = {
    title    : 'Interacciones',
    detail   : 'Lista de interacciones del sistema',
    resource : 'interaccion',
    rowLabel : 'nombre',
    icon :  'fa fa-link fa-fw', cargaExcel: true,
    onYadcfInit: inicializarFiltros
  };

  $scope.service = interaccionService;

  $scope.columns = [
    { title: 'Activo',    data: 'activoA.id', globalSearch: 'activoA.nombre',
      render: function(value, display, data) { return data.activoA.nombre;} ,
      filter_config: function (base) {
        return angular.extend(base, {
          data                : [{value: -1, label:'Cargando...'}]
        });
      }
    },
    { title: 'Activo',    data: 'activoB.id', globalSearch: 'activoB.nombre',
      render: function(value, display, data) { return data.activoB.nombre;} ,
      filter_config: function (base) {
        return angular.extend(base, {
          data                : [{value: -1, label:'Cargando...'}]
        });
      }
    },
    { title: 'Severidad', data: 'severidad.id', globalSearch: 'severidad.nombre',
      render: function(value, display, data) { return data.severidad.nombre;} ,
      filter_config: function (base) {
      return angular.extend(base, {
        data                : [{value: -1, label:'Cargando...'}]
      });
    }},
    { title: 'Efecto',    data: 'efecto'}
  ];

});

angular.module('sbAdminApp')
  .controller('InteraccionController', function($scope, $stateParams, $timeout, growl, $location, interaccionService, activoService, severidadService) {
    $scope.localModel = {};
    function configureActivoA(model) {

      if (!$scope.activosA) return;
      if (!model.activoA) return;

        var fromList = _.find($scope.activosA, function(value) { return value.id === model.activoA.id; });
        if (fromList) fromList.selected = true;
      $scope.localModel.activoA = fromList;
    }
    function configureActivoB(model) {

      if (!$scope.activosB) return;
      if (!model.activoB) return;

      var fromList = _.find($scope.activosB, function(value) { return value.id === model.activoB.id; });
      if (fromList) fromList.selected = true;
      $scope.localModel.activoB = fromList;
    }

    function configureActivos(model) {
      if($scope.activosA){
        configureActivoA(model);
        configureActivoB(model);
      }else{
        activoService.getAll().then(function data(data) {
          $scope.activosA = data.data;
          $scope.activosB = angular.copy($scope.activosA);
          configureActivoA(model);
          configureActivoB(model);
        });
      }
    }
    function configurarSeveridad(model) {
      if (!$scope.severidades) return;
      if (!model.severidad) return;
      var fromList = _.find($scope.severidades, function(value) { return value.id === model.severidad.id; });
      if (fromList) fromList.selected = true;
      $scope.localModel.severidad = fromList;
    }

    function configureSeveridades(model) {
      if($scope.severidades){
        configurarSeveridad(model);
      }else{
        severidadService.getAll().then(function data(data) {
          $scope.severidades = data.data;
          configurarSeveridad(model);
        });
      }
    }

    activoService.getAll().then(function data(data) {
      $scope.activosA = data.data;
      $scope.activosB = angular.copy($scope.activosA);
    });
    severidadService.getAll().then(function data(data) {
      $scope.severidades = data.data;
    });


    $scope.service = interaccionService;
    $scope.config = {
      title :'Interaccion',
      icon :  'fa fa-link fa-fw',
      preSubmit: function(model) {
        delete $scope.localModel.activoA.selected;
        delete $scope.localModel.activoB.selected;
        delete $scope.localModel.severidad.selected;
        model.activoA = $scope.localModel.activoA;
        model.activoB = $scope.localModel.activoB;
        model.severidad = $scope.localModel.severidad;
      },
      afterLoad: function(model){
        configureActivos(model);
        configureSeveridades(model);
      }
    };
    $scope.form = [
      {
        key         : 'efecto',
        type        : 'textarea',
        placeholder : 'Describa el efecto',
        title       : 'Efecto'
      }];
    /*
     validationMessage: {
     0: "Campo numérico" //numerico
     }
     */
    /*
    $scope.searchUsuarios = function(query) {
      if (query.length < 3) return;
      usuarioService.getByName(query).then(function ok(response) {
        $scope.usuarios = response.data;
      });

    };*/
    /* Configuracion general */
    $scope.isValid = function(name) {
      return ($scope.$$childHead.abmForm.$submitted && !$scope.activoA) ? true:false;
    };
  });
