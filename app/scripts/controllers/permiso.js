'use strict';


angular.module('sbAdminApp')
.controller('PermisoListController', function($scope,
                                             permisoService) {

  $scope.config = {
    title    : 'Permisos',
    detail   : 'Lista de permisos del sistema',
    resource : 'permiso',
    rowLabel : 'nombre'
  }

  $scope.service = permisoService;
  $scope.columns = [
    { title: 'Nombre'      , data: 'nombre' }     ,
    { title: 'Descripción' , data: 'descripcion'} ,
  ];

});

angular.module('sbAdminApp')
.controller('PermisoController', function($scope,
                                         permisoService) {

  $scope.service = permisoService;
  $scope.config = { title :"Permiso" };
  $scope.form = [
    {
      key         : "nombre",
      type        : "string",
      placeholder : "Nombre del permiso",
      title       : "Nombre",
    }, {
      key         : "descripcion",
      type        : "textarea",
      placeholder : "Descripción del permiso",
      title       : "Descripción"
    }];

});
