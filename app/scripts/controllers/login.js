'use strict';

angular.module('sbAdminApp')
.controller('LoginController', function($http, $window, $scope, growl, UserService) {

  /**
   * Carga el css del login
   */
  $scope.$on('$viewContentLoaded', function() {
    $scope._style = document.createElement('link');
    $scope._style.type = 'text/css';
    $scope._style.href = 'styles/login.css';
    $scope._style.rel = 'stylesheet';

    $scope._style = document.head.appendChild($scope._style);
  });

  $scope.$on('$destroy', function() {
    $scope._style.parentNode.removeChild($scope._style);
    delete $scope._style;
  });

  function init(){
    $scope.loginUser = {
      username : '',
      password : ''
    };
  }

  init();

  var lastMessage;
  function doLogin(reincidente) {
    var usuario = UserService.getNombre();
    if (lastMessage) lastMessage.destroy();
    if (reincidente) growl.success('Bienvenido de nuevo ' + usuario,  { ttl : 5000 });
      else growl.success('Bienvenido ' + usuario,  { ttl : 5000 });
    UserService.goToPersistLocation();
  }

  function wrongLogin(response) {
    response.errorHandled = true;
    var pass = $scope.loginUser.password;
    var detail = '';
    if (pass.toUpperCase() === pass) {
      detail = 'Ha ingresado una contraseña en mayúsculas.';
    }
    if (lastMessage) lastMessage.destroy();
    lastMessage = growl.error(detail, {
      ttl   : 5000,
      title : 'Usuario o contraseña inválida'
    });
  }

  if (UserService.lastUser()) {
    $scope.loginUser.username = UserService.lastUser();

    // verificar si el token sigue siendo válido
    if (localStorage.getItem('token')) {
      UserService.checkLogged(function() {
        doLogin(true);
      }, function error() {
        growl.warning('Su sesión expiro, inicie nuevamente por favor', { ttl : 5000 });
        $('#password').focus();
      });
    }

  }


  $scope.login = function(){

    UserService.login($scope.loginUser.username,
                      $scope.loginUser.password,
                      doLogin,
                      wrongLogin);

  };

});
