'use strict';

angular.module('sbAdminApp')
.controller('PacienteListController', function($scope, pacienteService) {

  $scope.config = {
    title    : 'Pacientes',
    detail   : 'Lista de pacientes del sistema',
    resource : 'paciente',
    rowLabel : 'nombre',
    icon :  'fa fa-users fa-fw'
  };

  $scope.service = pacienteService;

  $scope.columns = [
    { title: 'Código', data: 'codigo'},
    { title: 'Nombre'    , data: 'nombre', width: '80%'  }
  ];

});

angular.module('sbAdminApp')
  .controller('PacienteController', function($scope, $stateParams, $timeout, growl, $location, pacienteService) {

    console.log('PACIENTE_CONTROLLER');
    $scope.service = pacienteService;
    $scope.config = {
      title :'Paciente',
      icon :  'fa fa-users fa-fw'
    };
    $scope.form = [
      {
        key         : 'codigo',
        type        : 'number',
        placeholder : 'Código del paciente',
        title       : 'Código'
      },
      {
        key         : 'nombre',
        type        : 'string',
        placeholder : 'Nombre del paciente',
        title       : 'Nombre'
      }];
    /*
     validationMessage: {
     0: "Campo numérico" //numerico
     }
     */

  });
