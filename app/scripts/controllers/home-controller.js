/**
 * Created by sortiz on 13/01/17.
 */


'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:HomeController
 * @description
 * # HomeController
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('HomeController',  function ($scope, UserService, pacienteService, growl, medicamentoService, activoService, homeService, interaccionService) {

    var usuario = UserService.getCurrentUser();
    UserService.getById(usuario.id).then(
      function ok(response) {
      $scope.usuario = response.data;
    });

    pacienteService.getCount().then(
      function ok(response) {
        $scope.pacientes = response.data;
      });

    medicamentoService.getCount().then(
      function ok(response) {
        $scope.medicamentos = response.data;
      });

    activoService.getCount().then(
      function ok(response) {
        $scope.activos = response.data;
      });

    interaccionService.getCount().then(
      function ok(response) {
        $scope.interacciones = response.data;
      });

    homeService.setScope($scope);
    //homeService.prepararGraficos();
    //
    $scope.dateOptions = {
      formatYear  : 'yy',
      startingDay : 1,
      isOpen      : false
    };

    $scope.anhoDefault = (new Date()).getFullYear();
    $scope.anho = $scope.anhoDefault;

    var anhoInicio = 2000;
    $scope.anhos = [];

    var i = anhoInicio;
    for(i; i <= $scope.anho; i++){
      $scope.anhos.push(i);
    }

    $scope.meses = [];
    $scope.meses.push({label : 'Enero', value: 0});
    $scope.meses.push({label : 'Febrero', value: 1});
    $scope.meses.push({label : 'Marzo', value: 2});
    $scope.meses.push({label : 'Abril', value: 3});
    $scope.meses.push({label : 'Mayo', value: 4});
    $scope.meses.push({label : 'Junio', value: 5});
    $scope.meses.push({label : 'Julio', value: 6});
    $scope.meses.push({label : 'Agosto', value: 7});
    $scope.meses.push({label : 'Septiembre', value: 8});
    $scope.meses.push({label : 'Octubre', value: 9});
    $scope.meses.push({label : 'Noviembre', value: 10});
    $scope.meses.push({label : 'Diciembre', value: 11});

    var mesDefault = (new Date()).getMonth();

    angular.forEach($scope.meses, function(value) {
      if(value.value === mesDefault){
        $scope.mesDefault = value;
      }
    });

    $scope.mes = $scope.mesDefault;

    $scope.datos = {};
    $scope.onSelect = function () {
      var y = $scope.anho;
      var m = $scope.mes.value;

      var firstDay = new Date(y, m, 1);
      var lastDay = new Date(y, m + 1, 0);

      $scope.datos.desde = firstDay;
      $scope.datos.hasta = lastDay;

      App.blockUI({
        target: '#myChartObject',
        boxed : false,
        animate: true,
        iconOnly: false,
        textOnly: false,
        message: 'Buscando interacciones'
      });

      App.blockUI({
        target: '#activoFrecuente',
        boxed : false,
        animate: true,
        iconOnly: false,
        textOnly: false,
        message: 'Buscando interacciones'
      });

      pacienteService.getInteracciones($scope.datos).then(function(response) {
        $scope.interaccionesMasFrecuentes = response.data.masFrecuentes;

        $scope.activosMasFrecuentes = response.data.activosMasFrecuentes;

        homeService.prepararGraficos(response.data);

        App.unblockUI('#myChartObject');
        App.unblockUI('#activoFrecuente');
      }, function error(message) {
        growl.warning(message.data.mensaje);
        App.unblockUI('#myChartObject');
        App.unblockUI('#activoFrecuente');
      });
    };

    $scope.onSelect();

    $scope.desde = $scope.hasta = new Date();
    $scope.openDiaPickerDesde = function(){
      $scope.desdeIsOpen = true;
    };
    $scope.openDiaPickerHasta = function(){
      $scope.hastaIsOpen = true;
    };

  });


