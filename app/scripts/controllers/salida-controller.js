'use strict';

angular.module('sbAdminApp')
.controller('SalidaListController', function($scope, salidaService) {

  $scope.config = {
    title    : 'Salidas',
    detail   : 'Lista de salidas del sistema',
    resource : 'salida',
    rowLabel : 'nombre',
    icon : 'fa fa-list fa-fw', cargaExcel: true,
    deleteAll: true
  };

  $scope.service = salidaService;

  $scope.columns = [
    { title: 'Código', data: 'codigo', width: '10%'},
    { title: 'Paciente'    , data: 'paciente.nombre'},
    { title: 'Medicamento', data: 'medicamento.nombre'},
    { title: 'Cantidad', data: 'cantidad', width: '10%'},
    { title: 'Fecha', data: 'fecha', filter_type : 'range_date', width: '10%',
      className : 'fecha-filtro',
      render: function(data) {
        //return moment(data).format('DD/MM/YYYY HH:mm');
        return moment(data).format('DD/MM/YYYY');
      }}
  ];

});

angular.module('sbAdminApp')
  .controller('SalidaController', function($scope, $stateParams, $timeout, growl, $location, salidaService, activoService) {

    function configureActivos(model) {

      if (!$scope.activos) return;
      if (!model.activos) return;

      $scope.activos = model.activos;

    }

    $scope.service = salidaService;
    $scope.config = {
      title :'Salida', subtitle: 'Listado',
      icon : 'fa fa-list fa-fw',
      preSubmit: function(model) {
        console.log( $scope.activos);
        model.activos = $scope.activos;
      },
      afterLoad: configureActivos
    };
    $scope.form = [
      {
        key         : 'codigo',
        type        : 'number',
        placeholder : 'Código numérico de la salida',
        title       : 'Código'
      },
      {
        key         : 'fecha',
        placeholder : 'Fecha de la salida',
        title       : 'Fecha'
      },
      {
        key         : 'cantidad',
        placeholder : 'Cantidad de medicamentos',
        title       : 'Cantidad'
      }];
    /*
     validationMessage: {
     0: "Campo numérico" //numerico
     }
     */
    $scope.loadItems = function(query) {
      if(query.length > 0){
        return $scope.activosList.filter(function(activo) {
          return activo.nombre.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
      }
    };
    $scope.activosList = [];
    $scope.activos = [];
    activoService.getAll().then(
      function ok(response) {
        $scope.activosList = response.data;
        configureActivos($scope.config.model);
      });
  })
  .config(function(tagsInputConfigProvider) {
  tagsInputConfigProvider
    .setDefaults('tagsInput', {
      placeholder: 'Ingrese un activo'
    })
    .setDefaults('autoComplete', {
      highlightMatchedText: true
    });
});

angular.module('sbAdminApp')
.controller('SalidaDeleteController', function($scope, salidaService, $location, growl, $timeout) {

  var fecha = new Date();
  fecha.setHours(0);
  fecha.setMinutes(0);
  fecha.setSeconds(0);
  fecha.setMilliseconds(0);
  $scope.desde = fecha;
  $scope.hasta = fecha;

  $scope.aceptar = function () {
    $('#deleteModal').modal('show');
  };
  $scope.confirmar = function () {
    //$('#deleteModal').modal('show');
    salidaService.eliminarRango($scope.desde, $scope.hasta).then(
      function ok(response) {
        console.log(response);
        $('#deleteModal').modal('hide');
        growl.success('Se eliminaron ' + response.data + ' salidas', { title: 'Eliminados correctamente' });
        $timeout(function () {
          $scope.goList();

        }, 200);
      }, function error() {
        $('#deleteModal').modal('hide');
      });
  };
  $scope.goList = function() {
    var pathActual = $location.path();
    var newPath = pathActual.replace(/delete.*/i, 'list');
    console.log(newPath);
    $location.path(newPath);
  };
});
