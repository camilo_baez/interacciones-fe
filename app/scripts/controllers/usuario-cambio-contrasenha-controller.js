'use strict';

angular.module('sbAdminApp')
.controller('UsuarioChangePasswordController',
  function($scope, usuarioService, UserService, growl, $state, $stateParams) {

    $scope.canChangeAnyPassword = UserService.hasPermiso('CHANGE_ANY_USER_PASSWORD');

    $scope.data = {
      usuario : UserService.getCurrentUser(),
      isForce : UserService.getCurrentUser().cambiarContrasenha
    };

    if ($stateParams.id) {
      usuarioService.getById($stateParams.id).then(function(data) {
        $scope.data.usuario = data.data;
      });
    }


    $scope.searchUsuarios = function(query) {
      if (query.length < 3) return;
      usuarioService.getByName(query).then(function ok(response) {
        $scope.usuarios = response.data;
      });

    };

    $scope.cancelar = function() {
      if ($scope.canChangeAnyPassword) {
        $state.go('dashboard.home');
      }
    };

    $scope.cambiar = function() {
      if (!$scope.data.usuario) return;
      usuarioService.cambiarPass($scope.data.usuario.id, $scope.data).then(function() {
        UserService.set('cambiarContrasenha', false);
        UserService.getCurrentUser().cambiarContrasenha = false;
        growl.info('Contraseña cambiada correctamente');
        if ($scope.canChangeAnyPassword) {
          $state.go('dashboard.home');
        } else {
          UserService.logout();
          $state.go('login');
        }
      }, function(error) {
        growl.warning('Error ' + error.data.message);
      });
    };
  }
);
