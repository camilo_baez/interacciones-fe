/**
 * Created by psanchez on 13/01/17.
 */
'use strict';

angular.module('sbAdminApp')
  .controller('UsuarioMiCuentaController',
    function($scope, usuarioService, UserService, growl, $state, $stateParams,$timeout, $rootScope) {

      $scope.tipoDocumentos = [ 'CI', 'RUC', 'PASAPORTE' ];

      $scope.data = {
        usuario : UserService.getCurrentUser()
      };

      if ($stateParams.id) {
        usuarioService.getById($stateParams.id).then(function(data) {
          $scope.data.usuario = data.data;
        });
      }

      $scope.cancelar = function() {
          $state.go('dashboard.home');
      };

      $scope.guardar = function() {
        return usuarioService.updateOrAdd($scope.data.usuario).then(function(data) {
          console.log(data);
          $timeout(function() {
            $rootScope.$broadcast('changeUser', data);
          });
          growl.info('Guardado correctamente');
          $state.go('dashboard.home');
        });
      };
    }
  );
