'use strict';

angular.module('sbAdminApp')
.controller('DebugController', function($scope, DebugService, $timeout, growl) {

  var options = ['nginx', 'wildfly', 'db'];

  /**
   * Mapea una repuesta del servidor, a su representación
   * para la visualización (con iconos y clases).
   */
  function map(report) {
    if (!report) return;

    var toRet = [];

    _.each(options, function(e) {
      if (report[e].indexOf('Error') >= 0)
        toRet.push(e);
    });

    if (report.accesible.indexOf('Error') >= 0) {
      return {
        status : 'El sistema no esta accesible. Reiniciando.',
        'class': 'label-warning',
        'icon' : 'fa-bell-o',
        date   : report.date
      };
    }


    if (toRet.length === 0)
      return {
        status : 'Sistema disponible',
        'class': 'label-success',
        'icon' : 'fa-check',
        date   : report.date
      };
    else if (toRet.length === 1)
      return {
        status : 'El servicio ' + toRet.join(',') + ' no están accesibles. Iniciando servicios',
        'class': 'label-warning',
        'icon' : 'fa-bell-o',
        date   : report.date
      };
    else
      return  {
        status : 'Los servicios ' + toRet.join(',') + ' no están accesibles. Iniciando servicios',
        'class': 'label-warning',
        'icon' : 'fa-bell-o',
        date   : report.date
      };
  }

  /**
   * Convierte una lista de datos a su representación local, adicionalmente
   * permite filtrar el resultado
   */
  function convert(list, filter) {
    if (!filter) filter = '';
    var toFilter = filter.toLowerCase();
    return _(list)
      .map(map)
      .filter(function(e) {
        return (e.status + e.date).toLowerCase().indexOf(toFilter) >= 0;
      })
      .value();
  }


  $scope.$on('$viewContentLoaded', function() {
      // initialize core components
      App.initAjax();
      App.initSlimScroll('.scroller');

    });

  $scope.reload = function() {

    App.blockUI({
      target: '#main-table',
      boxed : true
    });

    DebugService.getDebugData().then(function(response) {
      $scope.data = response.data;
      $scope.data.data.unshift(response.data.last);
      $scope.realData = convert($scope.data.data, $scope.filter);

      $timeout(function() {
        App.unblockUI('#main-table');
      }, 500);
    });
  };

  $scope.$watch('filter', function(filter) {
    if (filter === undefined) return;
    $scope.realData = convert($scope.data.data, $scope.filter);
  });


  $scope.reload();




});

