'use strict';

angular.module('sbAdminApp')
.controller('MedicamentoListController', function($scope, medicamentoService) {

  $scope.config = {
    title    : 'Medicamentos',
    detail   : 'Lista de medicamentos del sistema',
    resource : 'medicamento',
    rowLabel : 'nombre',
    icon : 'fa fa-medkit fa-fw'
  };

  $scope.service = medicamentoService;

  $scope.columns = [
    { title: 'Código', data: 'codigo'},
    { title: 'Nombre'    , data: 'nombre'},
    //{ title: 'Descripción', data: 'descripcion'},
    { title: 'Activos', data: 'activos', filtrable: false, orderable: false, render: function(value, display, data) {
      var activos = '';
      angular.forEach(data.activos, function(valor, indice) {

        activos += valor.nombre;
        if(indice !== data.activos.length - 1){
          activos += ', ';
        }
      });
      return activos;
    }},
    { title: 'Vía' , data: 'via',
      render: function(value, display, data) {
        switch(data.via ) {
          case 'VO':
            return 'Oral (VO)';
          case 'IV':
            return 'Intravenosa (IV)';
          case 'IM':
            return 'Intramuscular (IM)';
          case 'SC':
            return 'Subcutánea (SC)';
          case 'VI':
            return 'Inhalatoria (VI)';
          case 'VR':
            return 'Rectal (VR)';
          case 'VT':
            return 'Tópica (VT)';
          case 'VV':
            return 'Vaginal (VV)';
          default:
            return 'N/A';
        }
      } ,
      filter_config : function(base) {
      return angular.extend(base, {
          data                : [
            {label:'Oral (VO)', value:'VO'},
            {label:'Intravenosa (IV)', value:'IV'},
            {label:'Intramuscular (IM)', value:'IM'},
            {label:'Subcutánea (SC)', value:'SC'},
            {label:'Inhalatoria (VI)', value:'VI'},
            {label:'Rectal (VR)', value:'VR'},
            {label:'Tópica (VT)', value:'VT'},
            {label:'Vaginal (VV)', value:'VV'}
            ]
        });
      }
    },
    { title: 'Controlado', data: 'controlado',  width: '10%',
      render: function(value, display, data) { return data.controlado === true? 'SI': 'NO' ;} ,
      filter_config: function (base) {
        return angular.extend(base, {
          data                : [{value: true, label:'SI'}, {value: false, label:'NO'}]
        });
      }}
  ];
});

angular.module('sbAdminApp')
  .controller('MedicamentoController', function($scope, $stateParams, $timeout, growl, $location, medicamentoService, activoService) {
    $scope.localModel = {};
    function configureActivos(model) {

      if (!$scope.activos) return;
      if (!model.activos) return;

      $scope.activos = model.activos;
      $scope.localModel.controlado = model.controlado;

    }

    $scope.service = medicamentoService;
    $scope.config = {
      title :'Medicamento',
      icon : 'fa fa-medkit fa-fw',
      preSubmit: function(model) {
        model.activos = $scope.activos;
        model.controlado = $scope.localModel.controlado;
      },
      afterLoad: configureActivos
    };
    $scope.form = [
      {
        key         : 'codigo',
        type        : 'number',
        placeholder : 'Código numérico del medicamento',
        title       : 'Código'
      },
      {
        key         : 'nombre',
        type        : 'string',
        placeholder : 'Nombre del medicamento',
        title       : 'Nombre'
      },
      {
        key         : 'descripcion',
        type        : 'string',
        placeholder : 'Descripcion del medicamento',
        title       : 'Descripcion'
      },
      {
        key         : 'via',
        placeholder : 'Vía de administración',
        title       : 'Vía',
        titleMap: [
          {name:'Oral (VO)', value:'VO'},
          {name:'Intravenosa (IV)', value:'IV'},
          {name:'Intramuscular (IM)', value:'IM'},
          {name:'Subcutánea (SC)', value:'SC'},
          {name:'Inhalatoria (VI)', value:'VI'},
          {name:'Rectal (VR)', value:'VR'},
          {name:'Tópica (VT)', value:'VT'},
          {name:'Vaginal (VV)', value:'VV'}
        ]
      }];
    /*
     validationMessage: {
     0: "Campo numérico" //numerico
     }
     */
    $scope.loadItems = function(query) {
      if(query.length > 0){
        return $scope.activosList.filter(function(activo) {
          return activo.nombre.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
      }
    };
    $scope.activosList = [];
    $scope.activos = [];
    activoService.getAll().then(
      function ok(response) {
        $scope.activosList = response.data;
        configureActivos($scope.config.model);
      });
  })
  .config(function(tagsInputConfigProvider) {
  tagsInputConfigProvider
    .setDefaults('tagsInput', {
      placeholder: 'Ingrese un activo'
    })
    .setDefaults('autoComplete', {
      highlightMatchedText: true
    });
});
