'use strict';


angular.module('sbAdminApp')
.controller('RolListController', function($scope,
                                             rolService) {

  $scope.config = {
    title    : 'Roles',
    detail   : 'Lista de roles del sistema',
    resource : 'rol',
    rowLabel : 'nombre'
  }

  $scope.service = rolService;
  $scope.columns = [
    { title: 'Nombre'      , data: 'nombre' }     ,
    { title: 'Descripción' , data: 'descripcion'}
  ];

});


angular.module('sbAdminApp')
.controller('RolController', function($scope, rolService, permisoService) {

  function configurePermissions(model) {

    if (!$scope.permisos) return;
    if (!model.permisos) return;

    _.each(model.permisos, function(data) {

      var fromList = _.find($scope.permisos, function(value) { return value.id === data.id; });
      if (fromList) fromList.selected = true;
    })

  }

  permisoService.getAll().then(function data(data) {
    $scope.permisos = data.data;
    configurePermissions($scope.config.model);
  });


  $scope.service = rolService;
  $scope.config = {
    title :"Rol",
    preSubmit: function(model) {

      model.permisos = _.filter($scope.permisos, function(permiso) { return permiso.selected; } );
    },
    afterLoad: configurePermissions
  };
  $scope.form = [
    {
      key         : "nombre",
      type        : "string",
      placeholder : "Nombre del Rol",
      title       : "Nombre",
    }, {
      key         : "descripcion",
      type        : "textarea",
      placeholder : "Descripción del rol",
      title       : "Descripción"
    }];

});
