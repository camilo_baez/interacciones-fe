'use strict';


angular.module('sbAdminApp')
.controller('UsuarioListController', function($scope, usuarioService, UserService, $state, growl) {

  $scope.config = {
    title    : 'Usuarios',
    detail   : 'Lista de usuarios del sistema',
    resource : 'usuario',
    rowLabel : 'nombre',
    withoutDelete : true,
    pushButton : function(buttonFactory, row) {

      var btns = [];
      if (UserService.hasPermiso('CHANGE_ANY_USER_PASSWORD')) {
        if (row.estado === 'ACTIVO')
          btns.push(buttonFactory('Desactivar', 'red', 'filter-cancel', 'times', 'remove'));
        btns.push(buttonFactory('Cambiar contraseña', 'blue', 'btn', 'key', 'change'));
        if (row.id !== 1000)
          btns.push(buttonFactory('Forzar cambio de contraseña en siguiente inicio de sesión', 'yellow', 'btn', 'power-off', 'force'));
      }

      return btns.join('');
    },
    onButtonClick : function(id, data) {
      if (id === 'change')
        $state.go('dashboard.usuario-change-pass', { id : data.id });
      if (id === 'force') {
        usuarioService.forceChange(data.id).then(function() {
          growl.info('En el siguiente inicio de sesión ' + data.nombre + ' tendra que cambiar su pass');
        });
      }

    }
  };

  $scope.service = usuarioService;
  $scope.columns = [
    { title: 'Nombre'   , data: 'nombre' },
    { title: 'Documento'   , data: 'documento'},
    { title: 'Correo'   , data: 'correo'},
    { title: 'Teléfono' , data: 'telefono'},
    { title: 'Estado' , data: 'estado', filter_config : function(base) {
          return angular.extend(base, {
            data                : [ 'ACTIVO', 'INACTIVO' ],
          });
        }
    }
  ];

});


angular.module('sbAdminApp')
.controller('UsuarioController', function($scope, growl, usuarioService, rolService) {

  /* Gestion de propiedades */
  function ordenarPropiedades(obj){
    var keys = [];
    var sorted_obj = {};

    for(var key in obj){
        if(obj.hasOwnProperty(key)){
            keys.push(key);
        }
    }

    keys.sort();

    jQuery.each(keys, function(i, key){
        sorted_obj[key] = obj[key];
    });

    angular.copy(sorted_obj, obj);
  }

  $scope.addPropiedad = function() {
    if($scope.config.propiedad){
      if($scope.config.model.configuraciones.hasOwnProperty($scope.config.propiedad)){
        growl.warning('La propiedad <b>' + $scope.config.propiedad + '</b> ya existe', { title: 'Propiedad duplicada', ttl : 5000 });
      }else{
        $scope.config.model.configuraciones[$scope.config.propiedad] = '';
        $scope.config.propiedad = '';
        ordenarPropiedades($scope.config.model.configuraciones);
      }
    }
  };

  $scope.removePropiedad = function(propiedad) {
    delete $scope.config.model.configuraciones[propiedad];
    ordenarPropiedades($scope.config.model.configuraciones);
  };

  $scope.isEmpty = function (obj) {
    for (var i in obj) if (obj.hasOwnProperty(i)) return false;
    return true;
  };

  rolService.getAll().then(function data(data) {
    $scope.roles = data.data;
  });

  /* Gestion de roles */
  function configureRoles(model) {
    if (!$scope.roles) return;
    if (!model.roles) return;
    _.each(model.roles, function(data) {
      var fromList = _.find($scope.roles, function(value) { return value.id === data.id; });
      if (fromList) fromList.selected = true;
    })
  }

  /* Gestion de tipo documento */
  function configureTipoDocumento(model) {
    if (!model.tipoDocumento) return;
    $scope.localModel.tipoDocumento = model.tipoDocumento;
  }

  $scope.listadoTipoDocumento = ['CI', 'RUC', 'PASAPORTE'];

  /* Configuracion general */
  $scope.isValid = function(name) {
    return ($scope.$$childHead.abmForm.$submitted && !$scope.localModel.tipoDocumento) ? true:false;
  };

  $scope.config = {
    title :"Usuario",
    preSubmit: function(model) {
      model.roles = _.filter($scope.roles, function(rol) { return rol.selected; } );
      model.tipoDocumento = $scope.localModel.tipoDocumento;
    },
    afterLoad: function(model){
      configureRoles(model);
      configureTipoDocumento(model);
    }
  };

  $scope.localModel = {};

  $scope.service = usuarioService;

  $scope.form = [
    {
      key         : 'username',
      type        : 'string',
      placeholder : 'Usuario para iniciar sesión',
      title       : 'Username',
    }, {
      key         : 'nombre',
      type        : 'string',
      placeholder : 'Nombre del Usuario',
      title       : 'Nombre', fieldHtmlClass: 'text-uppercase'
    }, {
      key         : 'direccion',
      type        : 'string',
      placeholder : 'Dirección del usuario',
      title       : 'Direccion'
    }, {
      key         : 'telefono',
      type        : 'string',
      placeholder : 'Teléfono del usuario',
      title       : 'Teléfono'
    }, {
      key         : 'correo',
      type        : 'string',
      pattern     : '^\\S+@\\S+$',
      placeholder : 'Correo electrónico del usuario',
      title       : 'Correo Electrónico'
    }, {
      key         : 'documento',
      type        : 'string',
      placeholder : 'Documento del usuario',
      title       : 'Documento'
    }];

});
