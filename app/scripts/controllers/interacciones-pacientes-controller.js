'use strict';

angular.module('sbAdminApp')
.controller('InteraccionesPacientesController', function(i18N, $scope, $stateParams, $timeout, growl, $location,
                                                         medicamentoService, pacienteService, interaccionesPacientesService, activoService) {

  $scope.datos = {};
  $scope.datos.viaDefault = {label: 'Todas', value: undefined};
  $scope.datos.altoRiesgoDefault = {label: 'Todas', value: undefined};
  $scope.datos.controladoDefault = {label: 'Todos', value: undefined};

  $scope.limpiar = function() {
    $scope.datos.via = $scope.datos.viaDefault;
    $scope.datos.altoRiesgo = $scope.datos.altoRiesgoDefault;
    $scope.datos.controlado = $scope.datos.controladoDefault;
    $scope.datos.paciente = undefined;
    $scope.datos.medicamento = undefined;
    var fecha = new Date();
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setSeconds(0);
    fecha.setMilliseconds(0);
    $scope.datos.desde = fecha;
    $scope.datos.hasta = fecha;

  };

  $scope.limpiar();

  $scope.vias = [ $scope.datos.viaDefault,
    {label:'Oral (VO)', value:'VO'},
    {label:'Intravenosa (IV)', value:'IV'},
    {label:'Intramuscular (IM)', value:'IM'},
    {label:'Subcutánea (SC)', value:'SC'},
    {label:'Inhalatoria (VI)', value:'VI'},
    {label:'Rectal (VR)', value:'VR'},
    {label:'Tópica (VT)', value:'VT'},
    {label:'Vaginal (VV)', value:'VV'}];
  $scope.riesgos = [ $scope.datos.altoRiesgoDefault, {label: 'Sólo D.A.R.', value: true}, {label: 'Sólo no D.A.R.', value: false}];
  $scope.controlados = [ $scope.datos.controladoDefault, {label: 'Sólo controlados', value: true}, {label: 'Sólo no controlados', value: false}];

  pacienteService.getAll().then(function(data) {
    $scope.pacientes = data.data;
  });

  medicamentoService.getAll().then(function(data) {
    $scope.medicamentos = data.data;
  });

  activoService.getAll().then(function(data) {
    $scope.activos = data.data;
  });

  var tabla, tablaPacientes,
    tablaInteraccionFrecuente, tablaActivoFrecuente,
    tablaSeveridadFrecuente;

  $scope.$on('$destroy', function() {
    interaccionesPacientesService.destroyTabla(tabla);
    interaccionesPacientesService.destroyTabla(tablaPacientes);
    interaccionesPacientesService.destroyTabla(tablaInteraccionFrecuente);
    interaccionesPacientesService.destroyTabla(tablaActivoFrecuente);
    interaccionesPacientesService.destroyTabla(tablaSeveridadFrecuente);
  });

  $scope.columns = [
    { title: 'Código',    data: 'codigoPaciente'},
    { title: 'Paciente',    data: 'paciente', filter_config: function (base) {
      return angular.extend(base, {
      });
    }},
    { title: 'Interaccion',    data: 'activos'},
    { title: 'Severidad',    data: 'severidad', filter_config: function (base) {
        return angular.extend(base, {
          });
      }
    },
    { title: 'Fecha', data: 'fecha', width: '10%',
      render: function(data) {
        //return moment(data).format('DD/MM/YYYY HH:mm');
        return moment(data).format('DD/MM/YYYY');
      }
    }
  ];

  $scope.columnsFrecuente = [
    {title: '#', data: '$index', order: false, filtrable: false, render: function (data, type, row, meta) {
      return meta.row + meta.settings._iDisplayStart + 1;
    }},
    { title: 'Nombre',    data: 'nombre'},
    { title: 'Cantidad',    data: 'cantidad'}
  ];

  $scope.columnsPacienteFrecuente = [
    {title: '#', data: '$index', order: false, filtrable: false, render: function (data, type, row, meta) {
      return meta.row + meta.settings._iDisplayStart + 1;
    }},
    { title: 'Código',    data: 'codigo'},
    { title: 'Nombre',    data: 'nombre'},
    { title: 'Cantidad',    data: 'cantidad'}
  ];

  $scope.configFrecuentes = {
    orderable:false,
    order: [[ 2, 'desc' ]]
  };

  var dibujarTabla = function (columns, tabla, config, data, oTable) {
    return interaccionesPacientesService.dibujarTabla(columns, tabla, config, data, oTable);
  };


  /*
   * Solicita los datos
   */
  $scope.aceptar = function() {
    App.blockUI({
      target: '#main-table',
      boxed : false,
      animate: true,
      iconOnly: false,
      textOnly: false,
      message: 'Buscando interacciones'
    });

    pacienteService.getInteracciones($scope.datos).then(function(response) {
      $scope.interaccionesMasFrecuentes = response.data.masFrecuentes;

      $scope.activosMasFrecuentes = response.data.activosMasFrecuentes;

      $scope.severidades = response.data.severidadMasFrecuentes;

      $scope.pacientesMasFrecuentes = response.data.pacienteMasFrecuentes;

      $scope.total = response.data.total;

      $scope.interacciones = response.data.interacciones;

      tabla = dibujarTabla($scope.columns, 'table', {orderable:true}, $scope.interacciones, tabla);
      tablaPacientes = dibujarTabla($scope.columnsPacienteFrecuente, 'tablaPacientes', $scope.configFrecuentes, $scope.pacientesMasFrecuentes, tablaPacientes);
      tablaInteraccionFrecuente = dibujarTabla($scope.columnsFrecuente, 'tablaInteraccionFrecuente',
        $scope.configFrecuentes, $scope.interaccionesMasFrecuentes, tablaInteraccionFrecuente);
      tablaActivoFrecuente = dibujarTabla($scope.columnsFrecuente, 'tablaActivoFrecuente',
        $scope.configFrecuentes, $scope.activosMasFrecuentes, tablaActivoFrecuente);
      tablaSeveridadFrecuente = dibujarTabla($scope.columnsFrecuente, 'tablaSeveridadFrecuente',
        $scope.configFrecuentes, $scope.severidades, tablaSeveridadFrecuente);

      App.unblockUI('#main-table');
      $scope.active = 1;
      if($scope.interacciones && $scope.interacciones.length > 0){
        //$('#collapse').click();
      }

    }, function error(message) {
      growl.warning(message.data.mensaje);
      App.unblockUI('#main-table');
      $scope.active = 1;
      //$('#collapse').click();
    });
  };
  $scope.getActivos = function (medicamento) {
    if(medicamento && medicamento.activos && medicamento.activos.length > 0){
      var activos = ' (';
      angular.forEach(medicamento.activos, function(valor, indice) {

        activos += valor.nombre;
        if(indice !== medicamento.activos.length - 1){
          activos += ', ';
        }
      });
      activos += ')';
      return activos;
    }else{
      return '';
    }

  };
});
