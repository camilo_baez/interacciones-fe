'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
var app = angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ngResource',
    'ngCookies',
    'ui.router',
    'ui.select',
    'ui.bootstrap',
    'angular-loading-bar',
    'schemaForm',
    'angular-growl',
    'ngMessages',
    'validation',
    'validation.rule',
    'cfp.hotkeys',
    'chart.js',
    'angularFileUpload',
    'summernote',
    'ngTagsInput',
    'ngFileUpload', 'countUpModule', 'googlechart', 'ui.toggle'
  ])
  .config(function(growlProvider) {

    // Configuración growl
    growlProvider.globalReversedOrder(true);
    growlProvider.onlyUniqueMessages(false);
    growlProvider.globalPosition('top-center');
    growlProvider.globalTimeToLive({
      success: 5000,
      error: -1,
      warning: 10000,
      info: 5000
    });
  })

  .config(function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider, $validationProvider, hotkeysProvider, $httpProvider) {

    hotkeysProvider.includeCheatSheet = true;
    hotkeysProvider.cheatSheetDescription = 'Mostrar / ocultar este menú';
    hotkeysProvider.templateTitle = 'Accesos directos';

    //Añadimos el Token para cada petición
    $httpProvider.interceptors.push('AuthInterceptor');
    $httpProvider.interceptors.push('ErrorInterceptor');



    //
    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

    //validationProvider.setValidMethod('blur');
    $validationProvider.setErrorHTML(function (msg) {
      return  '<label class="control-label has-error">' + msg + '</label>';
    });
    $validationProvider.showSuccessMessage = false;

    angular.extend($validationProvider, {
      validCallback: function (element){
                $(element).parents('.form-group:first').removeClass('has-error');

      },
      invalidCallback: function (element) {
                $(element).parents('.form-group:first').addClass('has-error');

      }

    });

    // Configuración de moment:
    moment.locale('es');

    //Configuracion del DatePicker, idioma
    ( function( factory ) {
      if ( typeof define === "function" && define.amd ) {

        // AMD. Register as an anonymous module.
        define( [ "../widgets/datepicker" ], factory );
      } else {

        // Browser globals
        factory( jQuery.datepicker );
      }
    }( function( datepicker ) {

      datepicker.regional.es = {
        closeText: 'Cerrar',
        prevText: '&#x3C;Ant',
        nextText: 'Sig&#x3E;',
        currentText: 'Hoy',
        monthNames: [ 'enero','febrero','marzo','abril','mayo','junio',
          'julio','agosto','septiembre','octubre','noviembre','diciembre' ],
        monthNamesShort: [ 'ene','feb','mar','abr','may','jun',
          'jul','ago','sep','oct','nov','dic' ],
        dayNames: [ 'domingo','lunes','martes','miércoles','jueves','viernes','sábado' ],
        dayNamesShort: [ 'dom','lun','mar','mié','jue','vie','sáb' ],
        dayNamesMin: [ 'D','L','M','X','J','V','S' ],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        changeMonth: true,
        changeYear: true,
        yearSuffix: '' };
      datepicker.setDefaults( datepicker.regional.es );

      return datepicker.regional.es;

    } ) );

    /*
     * Retorna una configuración para una vista de lista (por defecto).
     *
     */
    function getConfigList(resource, controller, usarBase) {
      var template = 'views/' + resource + '/list.html';
      if (usarBase) {
         template = 'views/default-table.html';
      }
      return {
        templateUrl : template,
        url         : '/' + resource + '/list',
        controller  : controller
      };
    }

    function getConfigAbm(resource, controller, usarBase) {

      var template = 'views/' + resource.replace('_', '-') + '/abm.html';
      if (usarBase) {
         template = 'views/schemaform-abm.html';
      }
      return {
        templateUrl : template,
        url         : '/' + resource + '/abm/:id?',
        controller  : controller,
      };
    }

    $urlRouterProvider.otherwise('/dashboard/home');

    $stateProvider
      .state('dashboard', {
        url:'/dashboard',
        templateUrl: 'views/dashboard/main.html'
    })
      .state('dashboard.home',{
        url:'/home',
        controller: 'HomeController',
        templateUrl:'views/dashboard/home.html'
      })
      .state('dashboard.form',{
        templateUrl:'views/form.html',
        url:'/form'
    })
      .state('dashboard.blank',{
        templateUrl:'views/pages/blank.html',
        url:'/blank'
    })
      .state('login',{
        templateUrl:'views/login.html',
        url:'/login',
        controller:'LoginController'
    })
      .state('dashboard.chart',{
        templateUrl:'views/chart.html',
        url:'/chart',
        controller:'ChartCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'bower_components/angular-chart.js/dist/angular-chart.min.js',
                'bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/chartContoller.js']
            });
          }
        }
    })
      .state('dashboard.table',{
        templateUrl:'views/table.html',
        url:'/table'
    })
      .state('dashboard.panels-wells',{
          templateUrl:'views/ui-elements/panels-wells.html',
          url:'/panels-wells'
      })
      .state('dashboard.buttons',{
        templateUrl:'views/ui-elements/buttons.html',
        url:'/buttons'
    })
      .state('dashboard.notifications',{
        templateUrl:'views/ui-elements/notifications.html',
        url:'/notifications'
    })
      .state('dashboard.typography',{
       templateUrl:'views/ui-elements/typography.html',
       url:'/typography'
   })
      .state('dashboard.icons',{
       templateUrl:'views/ui-elements/icons.html',
       url:'/icons'
   })
      .state('dashboard.grid',{
       templateUrl:'views/ui-elements/grid.html',
       url:'/grid'
   })
   // Usuarios
   .state('dashboard.usuario-list', {
     templateUrl : 'views/default-table.html',
     url         : '/usuario/list',
     controller  : 'UsuarioListController',
   })
   .state('dashboard.usuario-abm', {
     templateUrl : 'views/usuario/abm.html',
     url         : '/usuario/abm/:id?',
     controller  : 'UsuarioController',
   })
   .state('dashboard.usuario-change-pass', {
     templateUrl : 'views/usuario/change.html',
     url         : '/usuario/change/:id?',
     controller  : 'UsuarioChangePasswordController'
   })
   .state('dashboard.usuario-micuenta', {
     templateUrl : 'views/usuario/micuenta.html',
     url         : '/usuario/micuenta/:id?',
     controller  : 'UsuarioMiCuentaController'
   })
   // Permisos
   .state('dashboard.permiso-list', {
     templateUrl : 'views/default-table.html',
     url         : '/permiso/list',
     controller  : 'PermisoListController',
   })
   .state('dashboard.permiso-abm', {
     templateUrl : 'views/schemaform-abm.html',
     url         : '/permiso/abm/:id?',
     controller  : 'PermisoController',
   })
   // Roles
   .state('dashboard.rol-list', {
     templateUrl : 'views/default-table.html',
     url         : '/rol/list',
     controller  : 'RolListController',
   })
   .state('dashboard.rol-abm', {
     templateUrl : 'views/rol/abm.html',
     url         : '/rol/abm/:id?',
     controller  : 'RolController',
   })

   // Procesos y Actividades
      .state('dashboard.proceso-list', {
        templateUrl : 'views/proceso/buscadorProcesos.html',
        url         : '/proceso/abm',
        controller  : 'ProcesoListController'
      })


      .state('dashboard.proceso-resultado', {
        templateUrl : 'views/proceso/resultadoBusquedaProceso.html',
        url         : '/proceso/resultado',
        controller  : 'ProcesoResultadoController'
      })

      .state('dashboard.misprocesos', {
        templateUrl : 'views/proceso/misProcesos.html',
        url         : '/proceso/misprocesos',
        controller  : 'MisProcesosController'
      })

      .state('dashboard.misprocesos-view', {
        url: '/proceso/misprocesos/:id',
        templateUrl : 'views/proceso/miProcesoView.html',
        controller  : 'MisProcesosViewController'
      })

      .state('dashboard.misactividades-view', {
        url         : '/proceso/misprocesos/:procesoId/actividad/:id',
        templateUrl : 'views/proceso/miActividadView.html',
        controller  : 'MisActividadesViewController'
      })

      .state('dashboard.misprocesos-providenciar', {
        url         : '/proceso/misprocesos/:procesoId/actividad/:id/:accion',
        templateUrl : 'views/proceso/providenciarProceso.html',
        controller  : 'MisProcesosViewProvidenciarController'
      })

      .state('dashboard.procesos-entrantes-list', getConfigList('proceso', 'ProcesosEntrantesListController', true))
      .state('dashboard.procesos-entrantes-abm', {
        templateUrl : 'views/proceso/buscadorProcesos.html',
        url         : '/proceso/abm/:id?',
        controller  : 'ProcesoListController'
      })
      .state('dashboard.procesos-entrantes-change', {
        templateUrl : 'views/proceso/change.html',
        url         : '/proceso/list/:id?',
        controller  : 'ProcesosEntrantesController'
      })

   // Grupos
   .state('dashboard.grupo-list', getConfigList('grupo', 'GrupoListController', true))
   .state('dashboard.grupo-abm', {
     templateUrl : 'views/grupo/abm.html',
     url         : '/grupo/abm/:id?',
     controller  : 'GrupoController',
   })

   // Solicitante
   .state('dashboard.solicitante-list', getConfigList('solicitante', 'SolicitanteListController', true))
   .state('dashboard.solicitante-abm', {
     templateUrl : 'views/solicitante/abm.html',
     url         : '/solicitante/abm/:id?',
     controller  : 'SolicitanteController',
   })

    // Paciente
    .state('dashboard.paciente-list', getConfigList('paciente', 'PacienteListController', true))
      .state('dashboard.paciente-abm', {
        templateUrl : 'views/paciente/abm.html',
        url         : '/paciente/abm/:id?',
        controller  : 'PacienteController'
      })

    // Activo
    .state('dashboard.activo-list', {
      templateUrl : 'views/default-table.html',
      url         : '/activo/list',
      controller  : 'ActivoListController'
    })
      .state('dashboard.activo-abm', {
        templateUrl : 'views/activo/abm.html',
        url         : '/activo/abm/:id?',
        controller  : 'ActivoController'
      })

      // Severidad
      .state('dashboard.severidad-list', {
        templateUrl : 'views/default-table.html',
        url         : '/severidad/list',
        controller  : 'SeveridadListController'
      })
      .state('dashboard.severidad-abm', {
        templateUrl : 'views/schemaform-abm.html',
        url         : '/severidad/abm/:id?',
        controller  : 'SeveridadController'
      })
      // Salida
      .state('dashboard.salida-list', {
        templateUrl : 'views/default-table.html',
        url         : '/salida/list',
        controller  : 'SalidaListController'
      })
      .state('dashboard.salida-abm', {
        templateUrl : 'views/schemaform-abm.html',
        url         : '/salida/abm/:id?',
        controller  : 'SalidaController'
      })
      .state('dashboard.salida-delete', {
      templateUrl : 'views/salida/delete.html',
      url         : '/salida/delete/',
      controller  : 'SalidaDeleteController'
    })
    // Medicamento
    .state('dashboard.medicamento-list', {
      templateUrl : 'views/default-table.html',
      url         : '/medicamento/list',
      controller  : 'MedicamentoListController'
    })
    .state('dashboard.medicamento-abm', {
      templateUrl : 'views/medicamento/abm.html',
      url         : '/medicamento/abm/:id?',
      controller  : 'MedicamentoController'
    })
    .state('dashboard.interaccion-list', {
      templateUrl : 'views/default-table.html',
      url         : '/interaccion/list',
      controller  : 'InteraccionListController'
    })//Reportes
      .state('dashboard.interpacientes', {
        templateUrl : 'views/interpacientes/datos.html',
        url         : '/interpacientes/list',
        controller  : 'InteraccionesPacientesController'
      })
    .state('dashboard.interaccion-abm', {
      templateUrl : 'views/interaccion/abm.html',
      url         : '/interaccion/abm/:id?',
      controller  : 'InteraccionController'
    })
    ;


  }).run(function($rootScope, $state, UserService, growl, $timeout, historyService) {

    $rootScope.$on('$stateChangeStart', function(ev, toState, toParams) {
      if (!UserService.isLogged() && toState.name !== 'login') {
        UserService.persistLocation(toState.name, toParams);
        ev.preventDefault();
        $state.go('login');
      } else if(UserService.isLogged()){
          var permiso = toState.permiso ||
                    toState.controller.replace('Controller','').replace('List','').replace('Resultado','').replace('View','').replace('Providenciar','').replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase() + '_VIEW';
        if (permiso === 'ALL') return;
        if (permiso === 'SALIDA_DELETE_VIEW') return;

        if (UserService.hasPermiso(permiso) || toState.name === 'login' || toState.name === 'dashboard.usuario-change-pass' || toState.name === 'dashboard.home' || toState.name === 'dashboard.usuario-micuenta') {

            // tiene permisos, pero igual debe cambiar el pass
            if (UserService.getCurrentUser().cambiarContrasenha && toState.name !== 'dashboard.usuario-change-pass') {
              ev.preventDefault();
              $state.go('dashboard.usuario-change-pass');
              return;
            }

          } else {

            ev.preventDefault();
            growl.error('No posee permisos para utilizar esta funcionalidad.', { ttl : 5000 });
            $state.go('dashboard.home');

          }
      }
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams){
        historyService.add(toState, toParams);
    });

    $rootScope.$on('$viewContentLoaded', function() {
      $timeout(function() {
        Layout.setSidebarMenuActiveLink('match');
      });
    });

  });

  app.service('i18N', function() {
    return {
      buildDT : function(withSelect) {
        return {
          sProcessing     : 'Procesando...',
          sLengthMenu     : 'Mostrar _MENU_ registros',
          sZeroRecords    : 'No se encontraron resultados',
          sEmptyTable     : 'Ningún dato disponible en esta tabla',
          sInfo           : 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
          sInfoEmpty      : 'Mostrando registros del 0 al 0 de un total de 0 registros',
          sInfoFiltered   : '(filtrado de un total de _MAX_ registros)',
          sInfoPostFix    : '',
          sSearch         : 'Buscar : ',
          sUrl            : '',
          sInfoThousands  : ',',
          sLoadingRecords : 'Cargando...',
          oPaginate: {
            sFirst:    'Primero',
            sLast:     'Último',
            sNext:     'Siguiente',
            sPrevious: 'Anterior'
          },
          oAria: {
            sSortAscending:  ': Activar para ordenar la columna de manera ascendente',
            sSortDescending: ': Activar para ordenar la columna de manera descendente'
          },
          select: {
            rows : withSelect ? ' %d filas seleccionadas' : ''
          },
          buttons: {
            copyTitle: 'Copiado al portapapeles',
            copySuccess: {
              _: 'Copiados %d registros al portapapeles',
              1: 'Copiado 1 registro al portapapeles'
            },
          }
        };
      },


    };
  });

app.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 100 // auto scroll to top on page load
        },
        assetsPath: '../assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout',
    };

    $rootScope.settings = settings;
    return settings;
}]);

/* Setup App Main Controller */
app.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
app.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
app.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
app.controller('QuickSidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
       setTimeout(function(){
            QuickSidebar.init(); // init quick sidebar
       }, 2000);
    });
}]);

/* Setup Layout Part - Theme Panel */
app.controller('ThemePanelController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
app.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]).filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});
