'use strict';
angular.module('sbAdminApp')
.filter('moment', function($filter, $locale) {
  
  return function(amount, format) {
    if (amount !== 0 && !amount) return;
    if (!format) return moment(amount).format('YYYY-MM-DD');
    return moment(amount).format(format);
  };
});
