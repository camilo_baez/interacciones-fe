'use strict';
angular.module('sbAdminApp')
.filter('guarani', function($filter, $locale) {
  var currencyFilter = $filter('currency');
  var formats = $locale.NUMBER_FORMATS;
  return function(amount) {
    if (amount !== 0 && !amount) return;
    var value = currencyFilter(amount);
    var sep = value.indexOf(formats.DECIMAL_SEP);
    return value.substring(0, sep);
  };
});
