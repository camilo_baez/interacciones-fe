'use strict';

/*
 * Permite buscar un Usuario con un permiso dado
 *
 *
 * Configuración:
 *  permission : permiso requerido
 */
angular.module('sbAdminApp')
.directive('selectUsuario', function() {
  return {
    templateUrl : 'scripts/directives/select-usuario/select-usuario.html',
    restrict    : 'E',
    replace     : true,
    scope       : {
      permission  : '=',
      user        : '=',
      required    : '='
    },
    controller : function($scope, usuarioService) {

      $scope.model = { user : $scope.user };
      usuarioService.getWithPermission($scope.permission).then(function(data) {
        $scope.users = data.data;
      });

      $scope.selectUser = function(newModel) {
        $scope.user = newModel;
      };

    }
  };
});
