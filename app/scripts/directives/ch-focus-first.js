'use strict';

angular.module('sbAdminApp')
.directive( 'chFocusFirst', function ($timeout) {

  function selectFirst(element) {
    $timeout(function() {
      element.find('input,textarea,select').filter(':visible:first').focus();
    }, 500);
  }

  return {
    restrict : 'A',
    link     : function (scope, element) {
      scope.$watch(function() {
        return element.is(':visible');
      }, function onVisible() {
        selectFirst(element);
      });
    }
  };

});
