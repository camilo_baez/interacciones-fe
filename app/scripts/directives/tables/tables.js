'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 * Variables:
 *  columns : es un array de datatables que define las columnas, siempre debe
 *    tener una columna 'id' oculta.
 *  config: configuraciones de la vista;
 *    rowLabel : label al mostrar cuando se referencia un registro,
 *      default: 'id'
 *    dom : dom a representar al mostrar la tabla, ver datatables dom.
 *      default: 'lrtip'
 *    title : titulo de la vista (ej. Seguro)
 *    detail : descripción de la vista (ej. Seguros que trabajan con el hospital)
 *    resource : nombre de lo que se muestra (ej. seguro)
 *    withoutDelete : boolean que define si se muestra el botón eliminar.
 *    name : nombre para mostrar en la interfaz, por ejemplo "seguro".
 *           por defecto es igual a config.resource.
 *
 *    addLabel : texto a mostrar, por defecto es "Agregar {{ resource }}"
 *    addAction: accion invocada al presionar agregar, por defecto va a /abm.html
 *
 *    pushButton: función que puede retornar botones adicionales,
 *      recibe como parámetro una función que retorna botones con las siguientes
 *      opciones:
 *        clase    : clase
 *        text     : name
 *        icon     : icon
 *        keyClass : keyClass
 *    onButtonClick: funcion que recibe un id del boton presionado.
 *    select : permite que la tabla sea seleccionable
 *    imprimirVisibles : permite que la tabla sea seleccionable
 *    onYadcfInit : callback invocado al configurar yadcf.
 *
 */
angular.module('sbAdminApp')
.directive('list', function() {
  return {
    templateUrl : 'scripts/directives/tables/tables.html',
    restrict    : 'E',
    replace     : true,
    scope       : {
      columns : '=',
      config  : '=',
      service : '='
    },
    controller  : function($scope, $q, $location, growl, hotkeys, UserService, $timeout, i18N) {
      $scope.config.name = $scope.config.name || $scope.config.resource;

      if (angular.isDefined($scope.config.addLabel)) {
        $scope.addLabel = $scope.config.addLabel;
      } else {
        $scope.addLabel = 'Agregar ' + $scope.config.name;
      }

      function goAgregar(id) {
        if (angular.isFunction($scope.config.addAction)) {
          $scope.config.addAction(id);
          return;
        }
        var pathActual = $location.path();
        var newPath = pathActual.replace('list', 'abm') + '/';
        if (id) newPath += id;
        $location.path(newPath);
      }

      hotkeys
        .bindTo($scope)
        .add({
          combo       : 'ctrl+a',
          description : 'Agregar un ' + $scope.config.name,
          callback    : function(event) { goAgregar(); event.preventDefault(); }
        });

      function goEdit(data) {
        goAgregar(data.id);

      }

      function goDeleteAll() {
        var pathActual = $location.path();
        var newPath = pathActual.replace('list', 'delete') + '/';
        $location.path(newPath);
      }


      function goDelete(data) {
        $scope.toRemove = {
           label : data[ $scope.config.rowLabel || 'id' ],
           data : data
        };
        $('#deleteModal').modal('show');
      }

      function getButton(name, clase, keyClass, icon, id) {

        var template = _.template(
          '<button id="<%- id %>" type="button" class="btn btn-sm <%- clase %> " title="<%- text %>">' +
          '  <i class="fa fa-<%- icon %> "> </i>' +
          '</button>');
        return template({
          id       : id,
          clase    : clase,
          text     : name,
          icon     : icon
        });

      }

      var columnActions = {
        title     : 'Acciones',
        data      : null,
        className : 'button-column',
        sortable  : false,
        filtrable : false,
        render : function(data, type, full) {

          var canEdit = !$scope.config.withoutEdit && UserService.hasPermiso($scope.config.resource, 'EDIT');
          var canDelete = !$scope.config.withoutDelete && UserService.hasPermiso($scope.config.resource, 'DELETE');

          var buttons = [ '<div class="btn-group">' ];

          if (canEdit) {
            buttons.push(getButton('Editar', 'green', '', 'edit', 'edit'));
          }

          if (canDelete) {
            buttons.push(getButton('Borrar', 'red', 'filter-cancel', 'times', 'remove'));
          }

          if ($scope.config.pushButton) {
            buttons.push($scope.config.pushButton(getButton, full));
          }

          buttons.push('</div>');

          return buttons.join(' ');
        }
      };

      var canEdit = !$scope.config.withoutEdit && UserService.hasPermiso($scope.config.resource, 'EDIT');
      var canDelete = !$scope.config.withoutDelete && UserService.hasPermiso($scope.config.resource, 'DELETE');

      var cantidadAcciones = 0;
      if (canEdit) {
        cantidadAcciones++;
      }

      if (canDelete) {
        cantidadAcciones++;
      }

      if ($scope.config.pushButton) {
        cantidadAcciones++;
      }

      if(cantidadAcciones > 0)
        $scope.columns.push(columnActions);

      var filter = _.chain($scope.columns)
        .forEach(function(value, index) { value.idx = index; })
        .forEach(function(value) { value.defaultContent = ''; })
        .filter(function(value) { return value.filtrable || value.filtrable === undefined;})
        .map(function(value) {
          if (angular.isDefined(value.filter_config)) {
            return value.filter_config({
              column_number            : value.idx ,
              filter_reset_button_text : false,
              filter_default_label     : 'Todos...',
              select_type              : 'select2',
              style_class              : 'form-control select2',
              select_type_options      : { placeholder: 'Seleccione...',
                allowClear: true
              }
            });
          } else {
            return {
              column_number            : value.idx,
              filter_type              : value.filter_type || 'text',
              filter_default_label     : value.filter_type ? [ 'Desde', 'Hasta' ] : value.title,
              style_class              : [ 'form-control' ],
              filter_reset_button_text : false,
              filter_delay             : 700,
              date_format              : 'dd/mm/yyyy'
            };
          }
        }).value();


      var buttons = [{
        extend : 'colvis',
        text   : 'Columnas', className : 'btn green btn-outline dropdown-toggle dropdown'
      }/*, {
        text: 'Usr: ' + UserService.getNombre()
      }*/];
      if ($scope.config.cargaExcel) {
        buttons.unshift({
          titleAttr: 'Cargar excel',
          text:      'Cargar planilla',
          className: 'btn btn-default btn-outline yellow',
          action : function () {
            $('#excelModal').modal('show');
          }
        });
      }
      if ($scope.config.descargas) {
        buttons.unshift({
          className: 'btn btn-default',
          text:      '<i class="fa fa-file-excel-o font-green" aria-hidden="true"></i>',
          titleAttr: 'Descargar excel',
          action : function () {
            alert('Falta implementar');
          }
        });
      }

      if ($scope.config.imprimirVisibles) {
        buttons.push({
          extend : 'excel',
          text   : 'Imprimir Visibles'
        });
      }

      if ($scope.config.select) {
        buttons.unshift({
          extend : 'selectNone',
          className: 'btn yellow'});
        buttons.unshift({
          extend : 'selectAll',
          className: 'btn green',
          text   : 'Selecionar todos'
        });
      }
      //Configuracion para busqueda global
      var prepararBusqueda = function () {
        var enabled = $scope.config.globalSearchEnabled;
        if(enabled === undefined || enabled){
          if($scope.search && $scope.search !== ''){
            var campos = [];
            angular.forEach($scope.columns, function(columna) {
              var campo = columna.globalSearch || columna.data;
              if(campo)
                campos.push({nombre: campo});
            });
            return angular.toJson({ buscar: $scope.search, columnas: campos});
          }else{
            return '';
          }
        }else{
          return '';
        }


      };
      var lengthMenu = [[20], [20]];
      if (UserService.hasPermiso('MOSTRAR_MAS_REGISTROS_EN_TABLA')) {
        lengthMenu[0].push(50);
        lengthMenu[0].push(100);
        lengthMenu[0].push(250);

        lengthMenu[1].push(50);
        lengthMenu[1].push(100);
        lengthMenu[1].push(250);
      }

      var oTable = $('.table').DataTable({
        fnInitComplete: $scope.dtInstanceCallback,
        columns    : $scope.columns,
        lengthMenu : lengthMenu,
        columnDefs : [{
         responsivePriority: 1,
         targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        dom        : $scope.config.dom || 'Blprtip',
        order      : $scope.config.order || [[ 0, 'desc' ]],
        processing : true,
        serverSide : true,
        responsive : true,
        buttons    : buttons,
        select     : $scope.config.select || false,
        ajax       : $scope.service.loadDataInfo(),
        language   : i18N.buildDT($scope.config.select),
        search: {
                  search: prepararBusqueda
              }
      });

      $scope.$on('$destroy', function() {
        oTable.destroy();
      });

      $scope.recargar = function () {//para el globalSearch
        oTable.draw();
      };

      $scope.hasPermiso = function(accion){
        return UserService.hasPermiso($scope.config.resource, accion);
      };

      $scope.config.table = oTable;

      $scope.goAgregar = goAgregar;
      $scope.goDeleteAll = goDeleteAll;
      $scope.deleteMe = function() {
        $scope.service.remove($scope.toRemove.data.id).then(function ok(response) {
          $('#deleteModal').modal('hide');
          if(response.status === 200){
            growl.info('Registro eliminado correctamente');
            oTable.ajax.reload();
          }else{
            growl.error('No se puede eliminar, favor contacte con el administrador.');
            console.log(response);
          }
        });
      };

      $scope.excelUpload = function() {
          var datos = {files: $scope.files};
          $('#excelModal').modal('hide');
          return $scope.service.excelUpload(datos).then(function (response) {
            growl.info('Nuevos: ' + response.data.nuevos + '.' +
              '<br/>Actualizados: ' + response.data.actualizados + '.', {title: 'El excel fue cargado exitosamente.', ttl: 10000 });
            $scope.files = undefined;
            $scope.progress = 0;
            oTable.ajax.reload();

          }, function (response) {
            console.log('Error al guardar: ' + response);
            $scope.progress = 0;
          }, function (evt) {
            $scope.progress =
              Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
      };

      $('.table tbody').on('click', 'button', function (el) {
        var ss = $(el.target).parents('tr');
        var button = $(el.target).parents('button')[0];
        var row = oTable.row(ss).data();

        var element = button || el.target;
        if (element.id === 'edit') {
          goEdit(row);
        } else if (element.id === 'remove') {
          goDelete(row);
        } else {
          if ($scope.config.onButtonClick)
            $scope.config.onButtonClick(element.id, row);
        }
        el.preventDefault();
        // Como esto esta en un entorno jquery, hay que hacer el digest para
        // que se de cuenta que cambio algo.
        $scope.$apply();
      });


      //desaparece
      //$('.table thead').css('display', 'none');

      $('.table th').each(function() {
        var width = $(this).width();
        $(this).css('max-width', width + 'px');
      });


      yadcf.init(oTable, filter);
      yadcf.exResetFilters(oTable, [0,1,2,3,4,5,6,7,8,9,10], true);

      if (angular.isFunction($scope.config.onYadcfInit))
        $scope.config.onYadcfInit(oTable, filter);

      $(document).on('draw.dt', function () {
        $('.yadcf-filter-range-number').addClass('form-control');
        $('.yadcf-filter-range-number').addClass('form-control');
        $('.yadcf-filter-range-date').addClass('form-control');
        $('.yadcf-filter-range-date').addClass('form-control');
      });
    }
  };
});
