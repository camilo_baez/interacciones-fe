'use strict';

/*
 * Directiva para creación de formularios rápidamente
 *
 * Parámetros:
 *  form: detalles, del formulario, ver schema-form.
 *  service: servicio que debe tener
 *      getSchema: retorna el schema para validar.
 *      getById: recibe un long y debe retornar un modelo.
 *      updateOrAdd: recibe un modelo y lo persiste.
 *  config:
 *      preSubmit: opcional, método que es invocado antes
 *          de invocar a service#updateOrAdd.
 *      title: Título del formulario
 *      afterLoad: método invocado luego de que se carge
 *          el modelo a editar.
 *      onInvalidForm: método invocado cuando hay un error
 *          de validación.
 *
 */
angular.module('sbAdminApp')
.directive('schemaAbm', function() {

  return {
    templateUrl : 'scripts/directives/form/schemaAbm.html',
    restrict    : 'E',
    replace     : false,
    transclude  : true,
    scope       : {
      config  : '=',
      form    : '=',
      service : '='
    },
    controller : function($scope, growl, $stateParams, $location, hotkeys) {


      hotkeys
        .bindTo($scope)
        .add({
          combo       : 'ctrl+g',
          description : 'Guarda el ' + $scope.config.title,
          callback    : function(event) { $scope.submit(); event.preventDefault(); }
        }).add({
          combo : 'ctrl+x',
          description : 'Volver a la lista',
          callback : function(event) { $scope.cancel(); event.preventDefault(); }
        });

      function goList() {
        var pathActual = $location.path();
        var newPath = pathActual.replace(/abm.*/i, 'list');
        $location.path(newPath);
      }

      $scope.service.getSchema().then(function ok(response) {
        $scope.schema = response.data;
      });

      $scope.model = {};
      $scope.config.model = $scope.model;
      if ($scope.config.afterCreate) $scope.config.afterCreate($scope.model);
      $scope.base = {};
      $scope.cancel = goList;

      if ($stateParams.id) {
        $scope.service.getById($stateParams.id).then(function ok(data) {
          $scope.model = data.data;
          $scope.config.model = $scope.model;
          if ($scope.config.afterLoad) $scope.config.afterLoad($scope.model);
          $scope.base = JSON.parse(JSON.stringify(data.data));
        });
      }

      $scope.limpiar = function() {
        $scope.model = $scope.base;
        $scope.abmForm.$setPristine();
      };

      $scope.options = { validationMessage:
        { 302: 'Este campo es obligatorio',
          200: 'El texto es muy corto ({{viewValue.length}} caracter/es), mínimo {{schema.minLength}}',
          201: 'El texto es muy largo ({{viewValue.length}} caracter/es), máximo {{schema.maxLength}}',
          202: 'El texto no coincide con el patrón: {{schema.pattern}}'
        }
      };



      $scope.submit = function() {
        $scope.$broadcast('schemaFormValidate');

        if ($scope.abmForm.$valid) {
          if ($scope.config.preSubmit) $scope.config.preSubmit($scope.model);
          $scope.service.updateOrAdd($scope.model).then(function ok(data) {
            var link = $location.absUrl();
            if (!$scope.model.id)
              link += data.data.id;

            growl.success('Presiona <a href="' + link + '">aquí</a> para verlo', { title: 'Guardado correctamente' });
            goList();

          }, function failure(response) {
            if (response.data && response.data.error) {
              growl.error('Error al guardar, por favor intente más tarde', { title : response.data.error });
            } else {
              growl.error('Error al guardar, por favor intente más tarde');
            }
            response.errorHandled = true;
            console.log(response);
          });
        } else {
           if ($scope.config.onInvalidForm) $scope.config.onInvalidForm($scope.abmForm);
        }
      };
    }
  };
});
