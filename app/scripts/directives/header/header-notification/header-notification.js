'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
.directive('headerNotification',function() {
  return {
    templateUrl : 'scripts/directives/header/header-notification/header-notification.html',
    restrict    : 'E',
    replace     : true,
    controller  : function($scope, $state, $q, $window, growl, hotkeys, UserService, $rootScope) {

      $scope.logout = function() {

        growl.success('Sesión cerrada', { ttl : 10000 });
        UserService.logout();
        $state.go('login');
      };

      $scope.cambiarPass = function() {
        if (UserService.isLogged()) {
          $state.go('dashboard.usuario-change-pass', {id: UserService.getCurrentUser().id});
        }
      };

      $scope.myAccount = function() {
        if (UserService.isLogged()) {
          $state.go('dashboard.usuario-micuenta', {id: UserService.getCurrentUser().id});
        }
      };

      $scope.nombre = UserService.getNombre();
      $rootScope.$on('changeUser', function(event, data) {
        $scope.nombre = data.data.nombre;
      });

      hotkeys.bindTo($scope).add({
        combo       : 'ctrl+l',
        description : 'Cerrar sesión',
        callback    : function(event, hotkey) { $scope.logout(); event.preventDefault(); }
      });
    }
  };
});


