'use strict';

angular.module('sbAdminApp')
.directive( 'chShow', function (UserService) {

  function hideElement(element) {
    element.hide();
  }

  return {
    restrict : 'A',
    scope       : {
      permission : '='
    },
    link     : function (scope, element, attrs) {
      scope.$watch(attrs.chShow, function(value) {
        if(!UserService.hasPermiso(attrs.chShow)){
          hideElement(element);
        }
      });

    }
  };

});
