'use strict';

angular.module('sbAdminApp')
.directive( 'chFocus', function ($timeout, $rootScope) {

  var inputs = [];
  var baseElement;

  function process(element) {
    var elements = element.find('.ui-select-container, a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex="-1"], *[contenteditable]');

    elements.each(function(idx, el) {

      var qel = $(el);
      // Select 2
      if (qel.attr('tabindex') === '-1') return;
      if (qel.hasClass('ui-select-offscreen')) return;

      inputs.push(el);

    });

  }

  function focus(element) {

    var qel = $(element);

    if (qel.hasClass('ui-select-container')) {

      var toBroadcast = qel.attr('focus-on');
      if (toBroadcast) $rootScope.$broadcast(toBroadcast);
      else console.log('El elemento', element, 'no posee la propiedad focus-on, es infocuseable');
      return;
    }

    $timeout(function doFocus() { qel.focus(); });
  }

  function goNext() {
    process(baseElement);
    var el = document.activeElement;
    var qel = $(el);
    // si esto es un select2
    if (qel.hasClass('ui-select-search')) {
      // selecciono el host
      el = qel.parent()[0];
    }

    var idx = inputs.indexOf(el);
    focus(inputs[idx + 1]);
  }



  return {
    restrict : 'A',
    link     : function (scope, element) {

      baseElement = element;
      scope.$on('focusNext', function() {
        goNext();
      });

      $timeout(function() { process(element); });

      element.bind('keydown keypress', function(event) {
        if (event.which === 13) {
          if (document.activeElement &&
              document.activeElement.getAttribute('type') === 'submit') {
                return;
              }
              goNext();
              event.preventDefault();
        }
      });

    }
  };

});
