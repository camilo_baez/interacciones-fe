'use strict';

angular.module('sbAdminApp')
  .factory('homeService',  function () {
    var service = {};

    service.setScope = function (scope) {
      service.scope = scope;
      service.scope.cambiarTipoGrafico = function(tipo){
        service.scope.myChartObject.type = tipo;
      };
      service.scope.cambiarTipoGraficoActivos = function(tipo){
        service.scope.activoFrecuente.type = tipo;
      };
    };


    service.prepararGraficos = function(data) {
      var interacciones = data.masFrecuentes;
      var activos = data.activosMasFrecuentes;

      var rowsInteracciones = [];
      var rowsActivos = [];

      var i = 0;
      var j = 0;
      var limit = 10;
      if(interacciones.length < limit){
        limit = interacciones.length;
      }
      for(i ; i < limit ; i++){
        rowsInteracciones.push({c: [{v: interacciones[i].nombre}, {v: interacciones[i].cantidad}]});
      }

      limit = 10;

      if(activos.length < limit){
        limit = activos.length;
      }
      for(j ; j < limit ; j++){
        rowsActivos.push({c: [{v: activos[j].nombre}, {v: activos[j].cantidad}]});
      }


      service.scope.myChartObject = {};

      service.scope.myChartObject.type = 'PieChart';

        service.scope.myChartObject.data = {'cols': [
          {id: 't', label: 'Interacción', type: 'string'},
          {id: 's', label: 'Cantidad', type: 'number'}
        ], 'rows': rowsInteracciones};

        service.scope.myChartObject.options = {
          'showRowNumber' : true,
          'colors': ['#32c5d2', '#eb0068', '#13003c', '#594C76', '#1e767e'], //http://www.color-hex.com/color/32c5d2
          'enableInteractivity' : true
        };

      service.scope.activoFrecuente = {};

      service.scope.activoFrecuente.type = 'BarChart';

      service.scope.activoFrecuente.data = {'cols': [
        {id: 't', label: 'Activo', type: 'string'},
        {id: 's', label: 'Cantidad', type: 'number'}
      ], 'rows': rowsActivos};

      service.scope.activoFrecuente.options = {
        'showRowNumber' : true,
        'colors': ['#32c5d2', '#eb0068', '#13003c', '#594C76', '#1e767e'], //http://www.color-hex.com/color/32c5d2
        'enableInteractivity' : true
      };
    }
    return service;

});
