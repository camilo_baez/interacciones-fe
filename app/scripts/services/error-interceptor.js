'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
.factory('ErrorInterceptor', function ($window, $q, growl, $timeout) {
  return {

    response: function(response) {
      return response || $q.when(response);
    },

    responseError : function(response) {

      $timeout(function () {
        if (response.errorHandled) {
          return;
        }
        if (!response.data) {
          growl.error('Contacte con el administrador', { title : 'Error al realizar la operacion' });
          return;
        }
        if (response.data.error && response.data.detail) {
          // Muestra los errores de validacion.
          if (angular.isArray(response.data.detail)) {
            _.each(response.data.detail, function(error) {
              growl.error('El campo ' + error.property + ' ' + error.message, { title : response.data.error });
            });
          } else {
            // si es un mensaje generico
            growl.error(response.data.detail, { title : response.data.error });
          }
        } else if (response.data.error) {
          growl.error(response.data.error, { title : 'Error al realizar la operacion' });
        } else {
          growl.error('Contacte con el administrador', { title : 'Error al realizar la operacion' });
        }
      }, 0);
      return $q.reject(response);
    }
  };
});

