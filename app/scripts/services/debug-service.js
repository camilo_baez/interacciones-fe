'use strict';

angular.module('sbAdminApp')
  .factory('DebugService',  function (baseService) {

    var resource = 'debug';
    return {

      getDebugData : function() {
        return baseService.doGet(resource + '/log');
      },

    };

});
