'use strict';
/*
 * Servicio que se encarga de administrar la información de los usuarios.
 *
 *
 */
angular.module('sbAdminApp')
  .factory('UserService',  function (baseService, $state, $http, $cookies, usuarioService) {

    var currentUser = null;
    var resource = 'usuario';
    var fromPage;
    var fromParams;

    function get(key) {
      var toRet = localStorage.getItem(key);
      if (toRet === null) return null;
      if (toRet.indexOf('{') === 0) return JSON.parse(toRet);
      return toRet;
    }

    function set(key, value) {
      if (value === null) return;
      return localStorage.setItem(key, angular.isObject(value) ? JSON.stringify(value) : value);
    }

    function updateCredentials(id, user, token, nombre, roles, cambiarContrasenha) {
      currentUser = {
        id                 : id,
        name               : user,
        token              : token,
        roles              : roles,
        nombre             : nombre,
        cambiarContrasenha : cambiarContrasenha === true || cambiarContrasenha === 'true',
        permisos : _.chain(roles)
                    .flatMap(function(rol){
                      return rol.permisos;
                    })
                    .sortBy(function(e) {
                      return e.nombre;
                    }).value()
      };

      $.ajaxSetup({
        headers    : {
          Authorization : 'Bearer ' + token
        }
      });


      set('id', id);
      set('lastUser', user);
      set('token', token);
      set('name', nombre);
      set('roles', angular.toJson(roles));
      set('cambiarContrasenha', cambiarContrasenha);
      $cookies.put('Authorization', token);
    }

    function logout() {
      currentUser = null;
      localStorage.removeItem('token');
      $cookies.remove('Authorization');
      delete $http.defaults.headers.common.Authorization;
      delete $.ajaxSetup().headers.Authorization;
    }


    var currentSettings;
    var originalSettings;

    function defaultSettings() {
      return {
        defaultPrinter : null,
        currentPrinter : null,
        homePage       : 'dashboard.home'
      };
    }

    function setSettings(fromDbSettings) {

      set('settings', fromDbSettings);
      originalSettings = fromDbSettings || {};
      currentSettings = _.merge(defaultSettings(), originalSettings);

    }

    function updateSettings(newSettings) {

      var mergeSettings = _.merge(originalSettings, newSettings);
      setSettings(mergeSettings);
      originalSettings = mergeSettings;
      return usuarioService.updateSettings(currentUser.id, mergeSettings);
    }

    return {

      login : function(user, password, success, failure) {
        return baseService.doPost('authentication', {
          username : user,
          password : password
        }, {}, {
          'Content-Type' : 'application/x-www-form-urlencoded'
        }).then(function(response) {
          // TODO guardar toda la información del usuario

          if (response.status === 401) {
            if (failure) failure();
            return;
          }
          updateCredentials(response.data.user.id,
                            response.data.user.username,
                            response.data.token,
                            response.data.user.nombre,
                            response.data.user.roles,
                            response.data.user.cambiarContrasenha);
          setSettings(response.data.user.configuraciones);
          if (success) success();
        }, failure);
      },

      logout : logout,

      set : set,

      checkLogged : function(success, failure) {
        updateCredentials(get('id'), get('lastUser'), get('token'), get('name'), angular.fromJson(get('roles')), get('cambiarContrasenha'));
        setSettings(get('settings'));
        return baseService.doGet('authentication/isLogged').then(
          function stillLogged(response) {
            if (response.status === 401) {
              if (failure) failure();
              logout();
              return;
            }
            if (success) success();
          }, function notLogged() {
            if (failure) failure();
            logout();
        });
      },

      lastUser : function() {
        return localStorage.getItem('lastUser');
      },

      isLogged : function() {
        return currentUser !== null;
      },

      getToken : function() {
        return currentUser.token;
      },

      getUsername : function() {
         return currentUser.username;
      },

      getNombre : function() {
         return currentUser.nombre;
      },

      getCurrentUser : function(){
        return currentUser;
      },

      getById : function(id) { return baseService.getById(resource, id); },

      getRoles : function(){
        return currentUser.roles;
      },

      getGrupos : function(){
        return currentUser.grupos;
      },

      getPermisos : function(){
        return currentUser.permisos;
      },

      hasPermiso : function(permiso, accion){
        var permisos = currentUser.permisos;
        if (accion) permiso += '_' + accion;
        /*
          Si el permiso es undefined dejamos pasar.
         */
        return _.findIndex(permisos, function(o) { return o.nombre.toUpperCase().trim() === permiso.toUpperCase().trim().replace(/ /g, '_'); }) !== -1;
      },

      /**
       * Almacena una ubicación.
       *
       * Útil al inicio de sesión, ya que permite que luego se
       * redirija a la página donde trato de entrar inicialmente.
       */
      persistLocation : function(stateName, stateParams) {
        fromPage = stateName;
        fromParams = stateParams;
      },

      goToPersistLocation : function() {
        // Si no hay página de inicio, ir a consultas
        if (!fromPage) {
          $state.go(currentSettings.homePage);
          return;
        }
        $state.go(fromPage, fromParams);
      },

      getSettings : function() {
         return currentSettings;
      },

      updateConfig : function(newSettings) { return updateSettings(newSettings); }

    };

  });
