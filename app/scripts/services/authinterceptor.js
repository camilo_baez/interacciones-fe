'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
.factory('AuthInterceptor', function ($window, $q, $injector, growl) {
  return {
    request: function(config) {

      config.headers = config.headers || {};

      if (localStorage.getItem('token'))
        config.headers.Authorization = 'Bearer ' + localStorage.getItem('token');

      if (config.url.match('http://localhost/interacciones-service/'))
        delete config.headers.Authorization;

      return config || $q.when(config);

    },

    response: function(response) {
      return response || $q.when(response);
    },

    responseError : function(response) {
      if (response.status === 401) {
        // TODO: Redirect user to login page.
        var state = $injector.get('$state');
        var UserService = $injector.get('UserService');

        if (state.current.name !== 'login') {
          if(!response.data.notAllowed){
            UserService.persistLocation(state.current, state.params);
            state.go('login');
          }else{
            state.go('dashboard.home');
            growl.error('No posee permisos para utilizar esta funcionalidad.', { ttl : 5000 });
          }
        }
        response.errorHandled = true;
      }
      return $q.reject(response);
    }
  };
});

