'use strict';

/**
 * Servicio para poder irse hacia atras en el historial de estados.
 * Provee dos métodos:
 * add:    Para añadir el estado actual al historial
 * goBack: Transiciona al estado anterior. Borra el estado actual y el estado anterior del historial.
 *         Esto se debe a que al transicionar exitosamente, el estado anterior vuelve a añadirse
 *         al historial como el estado actual.
 */
angular.module('sbAdminApp')
  .factory('historyService',  function ($state) {
    var history = [];

    return {
      goBack: function () {
        if (history && history.length >= 2) {
          history.pop();
          var st = history.pop();
          $state.go(st.state.name, st.parameters);
        }
      },

      add: function (state, params) {
        var historyItem = {
          state: state,
          parameters: params
        };

        history.push(historyItem);
      }
    };
  });
