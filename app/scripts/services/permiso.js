'use strict';

angular.module('sbAdminApp')
  .factory('permisoService',  function (baseService) {

    var resource = 'permiso';
    return {

      getByName : function(query) {

        if (!query) query = '';
        return baseService.doGet(resource + '/filtrados/?nombre=' + query);
      },

      loadDataInfo : function(data, callback) { return baseService.loadDataInfo(resource, data, callback); },

      getSchema : function() { return baseService.doGet(resource + '/schema'); },

      getById : function(id) { return baseService.getById(resource, id); },

      getAll : function() { return baseService.getAll(resource); },

      updateOrAdd : function(model) { return baseService.updateOrAdd(resource, model); },

      remove : function(id) { return baseService.remove(resource, id); }
    };

});
