'use strict';

angular.module('sbAdminApp')
  .factory('interaccionesPacientesService',  function (baseService, i18N) {

    var lengthMenu = [[20], [20]];
    lengthMenu[0].push(50);
    lengthMenu[0].push(100);
    lengthMenu[0].push(250);

    lengthMenu[1].push(50);
    lengthMenu[1].push(100);
    lengthMenu[1].push(250);



    var destroyTabla = function (oTable) {
      if(oTable){
        oTable.destroy();
      }

    };

    var dibujarTabla = function(columns, tabla, config, data, oTable){
      var filter;

      if(data.length < 1){
        if(oTable){
          oTable.clear();
          oTable.draw();
        }
        return oTable;
      }

      filter = _.chain(columns)
        .forEach(function(value, index) { value.idx = index; })
        .forEach(function(value) { value.defaultContent = ''; })
        .filter(function(value) { return value.filtrable || value.filtrable === undefined;})
        .map(function(value) {
          if (angular.isDefined(value.filter_config)) {
            return value.filter_config({
              column_number            : value.idx ,
              filter_reset_button_text : false,
              filter_default_label     : 'Todos...',
              select_type              : 'select2',
              style_class              : 'form-control select2',
              select_type_options      : { placeholder: 'Seleccione...',
                allowClear: true
              }
            });
          } else {
            return {
              column_number            : value.idx,
              filter_type              : value.filter_type || 'text',
              filter_default_label     : value.filter_type ? [ 'Desde', 'Hasta' ] : value.title,
              style_class              : [ 'form-control' ],
              filter_reset_button_text : false,
              filter_delay             : 500,
              date_format              : 'dd/mm/yyyy'
            };
          }
        }).value();

      if(oTable){
        console.log(data);
        oTable.clear();
        angular.forEach(data, function(value) {
          console.log('a');
          oTable.row.add(value);
        });
        oTable.draw();
      }else{
        var buttons = [
          {
            extend: 'excel',
            className: 'btn btn-outline green',
            text:      '<span>excel</span>',
            titleAttr: 'Exportar a excel'
          },
          {
            extend: 'csv',
            className: 'btn btn-outline',
            text:      '<span>csv</span>',
            titleAttr: 'Exportar a csv'
          },
          {
            extend: 'copy',
            className: 'btn btn-outline',
            text:      '<span>copiar</span>',
            titleAttr: 'Copiar al portapapeles'
          }
        ];
        oTable = $('#'+tabla).DataTable({
          columns    : columns,
          lengthMenu : lengthMenu,
          columnDefs : [{
            responsivePriority: 1,
            targets: 0
          }, {
            responsivePriority: 2,
            targets: -1
          }],
          dom        : config.dom || 'Blprtip',
          ordering   : config.orderable || false,
          order      : config.order || [[ 0, 'desc' ]],
          processing : true,
          // serverSide : true,
          responsive : true,
          buttons    : buttons,
          select     : config.select || false,
          // ajax       : $scope.service.loadDataInfo(),
          language   : i18N.buildDT(config.select),
          data       : data
        });

        if (angular.isFunction(config.onYadcfInit))
          config.onYadcfInit(oTable, filter);


      }

      yadcf.init(oTable, filter);
      //yadcf.exResetFilters(oTable, [0,1,2,3,4,5,6,7,8,9,10], true);

      $(document).on('draw.dt', function () {
        $('.yadcf-filter-range-number').addClass('form-control');
        $('.yadcf-filter-range-number').addClass('form-control');
        $('.yadcf-filter-range-date').addClass('form-control');
        $('.yadcf-filter-range-date').addClass('form-control');
      });

      return oTable;
    };


    return {
      dibujarTabla : dibujarTabla,
      destroyTabla : destroyTabla
    };

});
