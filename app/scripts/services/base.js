'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .factory('baseService',  function ($http, $injector, $q, Upload) {

    var baseUrl;
    var baseUrlVue = "http://estadisticas.vue.org.py/exportador-service/";

    if (window.location.port === '8080')
      baseUrl = 'http://' + window.location.host + '/interacciones/rest/';
    else
      baseUrl = 'http://' + window.location.hostname + ':8080' + '/interacciones/rest/';

    function doGet(url, params) {
      return $http({
        method : 'GET',
        url    : baseUrl + url,
        params : params
      });
    }

    function doGetVue(url, params) {
      return $http({
        method : 'GET',
        url    : baseUrlVue + url,
        params : params
      });
    }

    function doPost(url, data, params, headers) {
      return $http({
        method  : 'POST',
        url     : baseUrl + url,
        data    : $.param(data),
        params  : params,
        headers : headers
      });
    }

	  /**
     * Wrapper de una función para inhabilitar la vista y mostrar un mensaje de espera durante
     * la ejecución de dicha función.
     *
     * @param func {Function} La función que recibe debe retornar un promise.
     * @returns {Function} Función durante cuya ejecución se inhabilitará la vista (hasta resolverse el promise)
     */
    function blockUiWrapper(func) {
      return function () {
        App.blockUI({ target: '#ui-view-content', boxed : true, message : 'Procesando ...' });
        var promise = func.apply(this, arguments);
        promise = promise.then(
          function success(response) { App.unblockUI('#ui-view-content'); return response; },
          function   error(reason) { App.unblockUI('#ui-view-content'); return $q.reject(reason); }
        );
        return promise;
      };
    }

    var toRet = {

      getBaseUrl : function() {
        return baseUrl;
      },

      doGet : function(url, params) { return doGet(url, params); },
      doGetVue : function(url, params) { return doGetVue(url, params); },
      doPost : doPost,
      doPut : function(url, data) {
        return $http.put(baseUrl + url, data);
      },
      doPostJson : function(url, data, params) {
        return $http.post(baseUrl + url, data, { params : params });
      },
      doDelete : function(url, data) {
        var config = {
          method: "DELETE",
          url: baseUrl + url,
          data: data,
          headers: {"Content-Type": "application/json;charset=utf-8"}
        };
        return $http(config);
      },

      loadDataInfo: function(resource) {
         return {
           url  : baseUrl + resource + '/',
           type : 'GET',
           error : function(error) {
             if (error.status === 401) {
               var state = $injector.get('$state');
               var UserService = $injector.get('UserService');
               if (state.current.name !== 'login') {
                 UserService.persistLocation(state.current, state.params);
                 state.go('login');
               }
             }

           }
         };
      },

      getById : function(resource, id) {
        return doGet(resource + '/' + id);
      },

      getAll : function(resource) {
        return doGet(resource + '/all');
      },

      updateOrAdd : function (resource, data) {
        if (!data.id) {
          return $http.post(baseUrl + resource, data);
        } else {
          return $http.put(baseUrl + resource + '/' + data.id, data);
        }
      },

      updateOrAddWithDocuments : function (resource, data) {
        if (!data.entidad.id) {
          return Upload.upload({
            url: baseUrl + resource + '/documentos',
            data: {files: data.files, entidad: Upload.json(data.entidad)}
          });
        } else {
          return Upload.upload({
            url: baseUrl + resource + '/documentos/'+data.entidad.id,
            method: 'PUT',
            data: {files: data.files, entidad: Upload.json(data.entidad)}
          });
        }
      },

      remove : function(resource, id) {
        return $http.delete(baseUrl + resource + '/' + id);
      },

      getCount : function(resource) {
        return $http.get(baseUrl + resource + '/count');
      },

      excelUpload : function (resource, data) {
        return Upload.upload({
          url: baseUrl + resource + '/excel',
          data: {files: data.files}
        });
      }
    };

    // Bloquear vista p/ acciones no-idempotentes
    toRet.updateOrAdd = blockUiWrapper(toRet.updateOrAdd);
    toRet.doPostJson =  blockUiWrapper(toRet.doPostJson);
    toRet.doPost =      blockUiWrapper(toRet.doPost);

    // Bloquear vista p/ acciones idempotentes (opcional)
    toRet.doPut =       blockUiWrapper(toRet.doPut);
    toRet.remove =      blockUiWrapper(toRet.remove);
    toRet.doDelete =    blockUiWrapper(toRet.doDelete);
    toRet.excelUpload = blockUiWrapper(toRet.excelUpload);

    return toRet;
  });


