'use strict';

angular.module('sbAdminApp')
  .factory('pacienteService',  function (baseService) {

    var resource = 'paciente';
    return {

      getByName : function(query) {
        if (!query) query = '';
        return baseService.doGet(resource + '/filtrados/?nombre=' + query);
      },

      getInteracciones : function(data) {
        var query = '';

        query += 'desde='+moment(data.desde).format('YYYY-MM-DD HH:mm:ss');
        query += '&hasta='+moment(data.hasta).format('YYYY-MM-DD HH:mm:ss');

        if(data.paciente)
        query += '&idPaciente='+data.paciente.id;
        if(data.medicamento)
          query += '&idMedicamento='+data.medicamento.id;
        if(data.activo)
          query += '&idActivo='+data.activo.id;
        if(data.via && angular.isDefined(data.via.value))
          query += '&via='+data.via.value;
        if(data.controlado && angular.isDefined(data.controlado.value))
          query += '&controlado='+data.controlado.value;
        if(data.altoRiesgo && angular.isDefined(data.altoRiesgo.value))
          query += '&riesgo='+data.altoRiesgo.value;

        return baseService.doGet('resultados/interacciones?' + query);
      },

      loadDataInfo : function(data, callback) { return baseService.loadDataInfo(resource, data, callback); },

      getSchema : function() { return baseService.doGet(resource + '/schema'); },

      getById : function(id) { return baseService.getById(resource, id); },

      getAll : function() { return baseService.getAll(resource); },

      updateOrAdd : function(model) { return baseService.updateOrAdd(resource, model); },

      remove : function(id) { return baseService.remove(resource, id); },

      getCount : function() { return baseService.getCount(resource); }
    };

});
