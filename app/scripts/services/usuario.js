'use strict';

angular.module('sbAdminApp')
  .factory('usuarioService',  function (baseService, $http) {

    var resource = 'usuario';
    return {

      getByName : function(query) {

        if (!query) query = '';
        return baseService.doGet(resource + '/filtered/?query=' + query);
      },

      loadDataInfo : function(data, callback) { return baseService.loadDataInfo(resource, data, callback); },

      getSchema : function() { return baseService.doGet(resource + '/schema'); },

      getById : function(id) { return baseService.getById(resource, id); },

      getAll : function() { return baseService.getAll(resource); },

      getWithGroup : function() {

        return baseService.doGet(resource + '/withGroup');

      },

      updateOrAdd : function (data) {

        if (!data.id) {
          return $http.post(baseService.getBaseUrl() + resource + '/addNoPass', data);
        } else {
          return $http.put(baseService.getBaseUrl() + resource + '/' + data.id, data);
        }
      },

      remove : function(id) { return baseService.remove(resource, id); },

      getPrinters : function() {
        return baseService.doGet('printer/');
      },

      updateSettings : function(id, newSettings) {
         return $http.put(baseService.getBaseUrl() + resource + '/' + id + '/configuraciones', newSettings);
      },

      cambiarPass : function(id, data) {
        return $http.post(baseService.getBaseUrl() + resource + '/' + id + '/changePassword', {
          newPass : data.password,
          oldPass : data.currentPassword,
          repeatNewPass : data.repeatPassword
        });
      },

      forceChange : function(id) { return baseService.doGet(resource + '/' + id + '/forceChange'); },

      getWithPermission : function(permission) { return baseService.doGet(resource + '/conPermiso?permiso=' + permission); }
    };

});
