'use strict';

angular.module('sbAdminApp')
  .factory('severidadService',  function (baseService) {

    var resource = 'severidad';
    return {

      getByName : function(query) {
        if (!query) query = '';
        return baseService.doGet(resource + '/filtrados/?nombre=' + query);
      },

      loadDataInfo : function(data, callback) { return baseService.loadDataInfo(resource, data, callback); },

      getSchema : function() { return baseService.doGet(resource + '/schema'); },

      getById : function(id) { return baseService.getById(resource, id); },

      getAll : function() { return baseService.getAll(resource); },

      updateOrAdd : function(model) { return baseService.updateOrAdd(resource, model); },

      remove : function(id) { return baseService.remove(resource, id); },

      getCount : function() { return baseService.getCount(resource); }
    };

});
