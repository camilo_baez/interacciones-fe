'use strict';

angular.module('sbAdminApp')
  .factory('salidaService',  function (baseService, $http) {

    var resource = 'salida';
    return {

      getByName : function(query) {
        if (!query) query = '';
        return baseService.doGet(resource + '/filtrados/?nombre=' + query);
      },

      eliminarRango : function(desde, hasta) {
        console.log(desde);
        console.log(hasta);
        return $http.delete(baseService.getBaseUrl() + resource + '/rango/' +
          moment(desde).format('YYYY-MM-DD HH:mm:ss') + '/' +
          moment(hasta).format('YYYY-MM-DD HH:mm:ss'));
      },


      loadDataInfo : function(data, callback) { return baseService.loadDataInfo(resource, data, callback); },

      getSchema : function() { return baseService.doGet(resource + '/schema'); },

      getById : function(id) { return baseService.getById(resource, id); },

      getAll : function() { return baseService.getAll(resource); },

      updateOrAdd : function(model) { return baseService.updateOrAdd(resource, model); },

      remove : function(id) { return baseService.remove(resource, id); },

      excelUpload : function (data) {return baseService.excelUpload(resource,data); },

      getCount : function() { return baseService.getCount(resource); }

    };

});
