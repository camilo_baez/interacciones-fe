#!/usr/bin/env bash

#sudo setfacl -R -m u:sortiz:rwx /var/www/html

set -e

grunt clean build --force

mkdir -p dist/bower_components/datatables/media/images
cp app/images/sort_desc.png dist/bower_components/datatables/media/images/
cp app/images/sort_desc_disabled.png dist/bower_components/datatables/media/images/
cp app/images/sort_asc.png dist/bower_components/datatables/media/images/
cp app/images/sort_asc_disabled.png dist/bower_components/datatables/media/images/
cp app/images/sort_both.png dist/bower_components/datatables/media/images/

rm -rf /var/www/html/interacciones

cp -r dist/ /var/www/html/interacciones

echo "Script ejecutado correctamente."
